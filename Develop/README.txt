July 25th, 2019
Authors: Dario Rodrigues, Matias Senger
psamp_optimizer.py makes a sweep in psamp values in order to find the minimum noise.
It scripts also calculate the gain for each psamp and plot noise vs. psamp.
It can aso be used to determine the Dark Current choosing an Exposure Time (i.e. 1 hr)
and modifing the input file with the best psamp.
 
