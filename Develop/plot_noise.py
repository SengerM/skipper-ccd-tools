import matplotlib.pyplot as plt
import numpy as np

data_stdv = np.genfromtxt('output_noises.txt', comments = '#').transpose()
data_gain = np.genfromtxt('output_gains.txt', comments = '#').transpose() 

stdv = [None]*4
gain = [None]*4
psamp = [None]*4
noise = [None]*4

fig, ax = plt.subplots()

for k in [0, 1, 2, 3]:
    stdv[k] = data_stdv[1][data_stdv[0] == k+1]
    gain[k] = data_gain[2][data_gain[1] == k+1]
    psamp[k] = data_gain[0][data_gain[1] == k+1]
    print(psamp[k])
    noise[k] = np.array(stdv[k])/np.array(gain[k])
    print(noise[k])
    ax.plot(psamp[k],noise[k], marker = '.',label = 'quadrant ' + str(k+1))

ax.legend()
plt.xlabel('gain')
plt.ylabel('Noise [electrons]')
plt.ylim((0,10))
fig.savefig('plot_noise.pdf')
