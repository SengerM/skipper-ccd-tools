## Please, check the right size of the CCD and the OVERSCAN desired. 
## Also choose the NROWS and NSAMPS values for the Analysis.

ACTIVE_AREA_ROWS = 4126
ACTIVE_AREA_COLS = 886
ROW_OVERSCAN = 100
COL_OVERSCAN = 100

#NROWS=int(ACTIVE_AREA_ROWS/2)  # Number of Rows to be considerer for noise calculation
NROWS=200
NSAMPS=400  # Number of skipper measurements to be performed

EXPOSURE_TIME = 3600 # seconds

#########################################################

PSAMP_CLK = 15e6
DELAY_INTEG_WIDTH_CLK = 100e6
DEAD_TIME = 4000/DELAY_INTEG_WIDTH_CLK - 189/PSAMP_CLK # Dead time in seconds

#########################################################

import os
import ltapy
import numpy as np
from time import sleep
from bureaucracy import *

#########################################################

PSAMP_VALS = np.genfromtxt('input_psamp.txt', comments = '#').transpose()
PSAMP_VALS=[int(p) for p in PSAMP_VALS]

print('psamp values from input_psamp.txt that will be used for sweeping')
print(PSAMP_VALS)
print('')

print('Rows in the Active Area:     ',ACTIVE_AREA_ROWS)
print('Columns in the Active Area:  ',ACTIVE_AREA_COLS)
print('Rows in the Row OverScan:    ',ROW_OVERSCAN)
print('Columns in the Col OverScan: ',COL_OVERSCAN)
print('Number of rows to analyze:   ',NROWS)

###########################################################

print('Starting script!')

reading_directory = get_reading_dir(dirname = input('Insert title for this measurement: ').replace(' ', '_'), current_script_path = os.path.realpath(__file__))

lta = ltapy.lta(reading_directory=reading_directory)
lta.do('NROW ' + str(int(ACTIVE_AREA_ROWS/2) + ROW_OVERSCAN))
lta.do('NCOL ' + str(int(ACTIVE_AREA_COLS/2) + COL_OVERSCAN))

print('Reading pre erase and purge...')
lta.read(reading_name = 'pre_eAp')

print('Erasing and purging...')
lta.erase_and_purge()

print('Cleaning the CCD...')

print('Reading post erase and purge...')
lta.read(reading_name = 'post_eAp')

print('Reading post post erase and purge...')
lta.read(reading_name = 'post_post_eAp')

for psamp in PSAMP_VALS:
    
    print('psamp value: ' + str(psamp))
    lta.do('set psamp ' + str(psamp))
    lta.do('set ssamp ' + str(psamp))
    delay_Integ_Width = int((DEAD_TIME + float(psamp)/PSAMP_CLK)*DELAY_INTEG_WIDTH_CLK)
    lta.do('delay_Integ_Width ' + str(delay_Integ_Width))
    print('delay_Integ_Width :' + str(delay_Integ_Width))
    
    print('')
    print('Exposing for ' + str(EXPOSURE_TIME) + ' seconds')
    sleep(EXPOSURE_TIME)

    print('')
    print('Reading with psamp = ' + str(psamp))

    lta.read(timestamp=False,
        reading_name = 'psamp_' + str(psamp),
        NROW = int(NROWS),
        NCOL = int(ACTIVE_AREA_COLS/2 + COL_OVERSCAN),
        NSAMP = int(NSAMPS)
        )

########################################################################

#COL_PRESCAN = 7

#print('The noise is calculated in: ['+str(int(COL_PRESCAN + ACTIVE_AREA_COLS/2)) + ':' + str(int(COL_PRESCAN + ACTIVE_AREA_COLS/2 + COL_OVERSCAN - 10)) + ',1:' + str(int(NROWS))+']')

#for psamp in PSAMP_VALS: 
#    os.system('./noisamp.exe psamp_'+ str(psamp)+'.fits -p -q [' + str(int(COL_PRESCAN + ACTIVE_AREA_COLS/2)) + ':' + str(int(COL_PRESCAN + ACTIVE_AREA_COLS/2 + COL_OVERSCAN - 10)) + ',1:' + str(int(NROWS)) + '] >> output_noises.txt')
