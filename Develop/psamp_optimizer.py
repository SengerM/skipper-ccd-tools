import numpy as np
import os

zero=1000     # roughly estimation of the position of the peak for one electrons
stdv=80       # roughly estimation of standar deviation for the peak for zero electrons

SKIPPER2ROOT_DIR='/home/darmat/soft/skipper2root/'  # Do not forget the / at the end!

os.system('python3 Noise_Scan.py')
os.system('ls -t *.fits | xargs -n1 '+ SKIPPER2ROOT_DIR +'skipper2root.exe -i -a2.5 -b')
os.system('python3 noisamp.py')

PSAMP_VALS  = np.genfromtxt('input_psamp.txt', comments = '#').transpose()
PSAMP_VALS=[int(p) for p in PSAMP_VALS]

for psamp in PSAMP_VALS: # psamp will be used by calibrator2.exe as an estimation of gain
	for k in range(4): # number of quadrant to be analized
		os.system('./calibrator2.exe psamp_' + str(psamp) + '.root ' + str(psamp) + ' ' + str(zero) + ' ' + str(stdv))

os.system('python3 plot_noise.py')
