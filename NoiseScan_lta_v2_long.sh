#!/bin/bash

#Initialization
#initiate LTA and set default parameters for skipper
source /home/lta-test/ltaDaemon-DarMat/init_skp_lta_v2.sh
rootFolder=/home/lta-test/ltaDaemon-DarMat

lta NROW 750
lta NCOL 500

#erase and e-purge detector
lta exec ccd_erase
lta exec ccd_epurge
lta set vsub 40

#Reading CCD to clean
lta name $rootFolder/images/borrar

for i in `seq 1 4`
do
lta read
done

rm $rootFolder/images/borrar*

lta NSAMP 300

#set parameters and noise-scan loop
Pinit=(0)
Sinit=(65)
#Samp=(7 8 9 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90 95 100 150 189 200 250 300 350 400 450 500 550 600)
Samp=(20 40 60  80 90 100 150 189 200 250 300 400 500 600)


for i in "${Pinit[@]}"
do
	for j in "${Sinit[@]}"
	do
		for k in "${Samp[@]}"  
		do
			echo loop $i $j $k
			echo pinit = $i
			echo sinit = $j
			echo Samp = $k
			lta name $rootFolder/images/noise_scan/Scan_PINIT${i}_SINIT${j}_SAMP${k}_
			lta set pinit ${i}
			lta set sinit ${j}
			lta set ssamp ${k}
			lta set psamp ${k}
			lta read
		done
	done
done
