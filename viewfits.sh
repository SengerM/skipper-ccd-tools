#!/bin/bash

########################################################################
#
# This script is intended to open one ".fits" file that contains 4 images
# corresponding to each quadrant of the CCD. It automatically rotates and
# accomodates the images to restore the actual position in the active
# area of the CCD.
# 
# Usage
# -----
# 
# sh viewfits.sh <my_measurement.fits>
# 
########################################################################

file=$1

ds9 -multiframe ${file} -rotate 90 -zoom to fit -scale histequ -match scale -frame match physical -frame 2 -frame move last -frame 4 -orient y -frame 1 -frame move forward -orient x -frame 3 -orient xy
