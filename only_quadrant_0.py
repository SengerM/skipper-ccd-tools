import os
import sys
from astropy.io import fits
import numpy as np
import argparse

def process_file(directory, file):
	with fits.open(directory + file) as hdulist:
		new_hdul = fits.PrimaryHDU(hdulist[0].data)
		new_hdul.header = hdulist[0].header
		new_hdul.writeto(directory + file[:-5] + '-Quadrant_0.fits')

########################################################################

parser = argparse.ArgumentParser(description='Throw away all quadrants except for the number 0 one.')
parser.add_argument('-i',
					metavar = 'fileORdirectory', 
					help = 'Path to a FITS file or a directory containing FITS files to process. It is assumed that the FITS files header has the format from the LTA daemon.',
					required = True,
					dest = 'file_or_directory'
					)



args = parser.parse_args()

directory_or_file = args.file_or_directory

if '.fits' in directory_or_file: # In this case this is just a file
	process_file(
				  directory = directory_or_file[:directory_or_file.rfind('/')+1],
				  file = directory_or_file[directory_or_file.rfind('/')+1:]
				)
else: # We have to process all the directory
	if directory_or_file[-1] != '/':
		directory_or_file += '/'
	for file in os.listdir(directory_or_file):
		if '.fits' in file:
			process_file(directory = directory_or_file, file = file)
