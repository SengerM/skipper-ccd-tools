import os
import sys
from astropy.io import fits
import numpy as np
import matplotlib.pyplot as plt
import argparse

def process_file(path, file):
	with fits.open(path + file) as hdul:
		hdul[0].data[hdul[0].data>20e3] = 0
		hdul.writeto(path + file[:-5] + '_muons_removed.fits')

########################################################################

parser = argparse.ArgumentParser(description='Places a mask on muons (and other traces) from a FITS image.')
parser.add_argument('--mask-in',
					metavar = 'file', 
					help = 'Path to the FITS file in which you want to mask the muons.',
					required = True,
					dest = 'remove_from_file_name'
					)
parser.add_argument('--look-in',
					metavar = 'file', 
					help = 'Path to the FITS file in which to look for the muons. If not passed then "--mask-in" file is used. This option is useful when you can subtract the signal.',
					dest = 'median_subtracted_file_name',
					default = None
					)
parser.add_argument('--mask-radius',
						metavar = 'radius', 
						help = 'Radius of mask for clearance in pixels. Default is 2.',
						dest = 'mask_radius',
						type = float,
						default = 2
						)
parser.add_argument('--umbral',
						metavar = 'umbral',
						help = 'All pixels with more value than this umbral will be considered part of a muon. Default is 100e3. If "--median-subtracted-fits" file is provided this umbral is compared against the fluctuations from zero.',
						dest = 'umbral',
						type = float,
						default = 100e3
						)
parser.add_argument('--replace-with',
						metavar = 'value', 
						help = 'Numerical value to write in pixels that contains muons. Default is nan. This argument can be anything that can be casted to a float, e.g. "--replace-with inf" will place an infinite value for each muon.',
						dest = 'replace_with',
						default = 'nan'
						)

args = parser.parse_args()

remove_from_path = args.remove_from_file_name[:args.remove_from_file_name.rfind('/')+1]
remove_from_fname = args.remove_from_file_name[args.remove_from_file_name.rfind('/')+1:]

mask = np.ones([2*(int(np.ceil(args.mask_radius))+1)-1]*2)
for i in range(mask.shape[0]):
	for j in range(mask.shape[1]):
		if (i-args.mask_radius)**2 + (j-args.mask_radius)**2 <= args.mask_radius**2:
			mask[i,j] = 0

with open(remove_from_path + remove_from_fname[:-5] + '-muons_indices.txt', 'w') as aaaa:
	print('# Indices of image "' + remove_from_fname + '" where muons were encountered.', file = aaaa)
	print('# HDU_number,row,col', file = aaaa)

with fits.open(remove_from_path + remove_from_fname) as remove_from_hdul:
	for k_hdu in range(len(remove_from_hdul)):
		data = np.ones([
						 remove_from_hdul[k_hdu].data.shape[0] + 4*mask.shape[0], 
						 remove_from_hdul[k_hdu].data.shape[1] + 4*mask.shape[1]
						])
		data[
			 2*mask.shape[0] : 2*mask.shape[0] + remove_from_hdul[k_hdu].data.shape[0],
			 2*mask.shape[1] : 2*mask.shape[1] + remove_from_hdul[k_hdu].data.shape[1],
			] *= remove_from_hdul[k_hdu].data
		
		if args.median_subtracted_file_name == None:
			indices_of_muons = np.argwhere(remove_from_hdul[k_hdu].data > args.umbral)
		else:
			with fits.open(args.median_subtracted_file_name) as median_subtracted_hdul:
				indices_of_muons = np.argwhere(median_subtracted_hdul[k_hdu].data**2 > args.umbral**2)
		
		for muon_indices in indices_of_muons:
			with open(remove_from_path + remove_from_fname[:-5] + '-muons_indices.txt', 'a') as aaaa:
				print(str(k_hdu) + ',' + str(muon_indices[0]) + ',' + str(muon_indices[1]), file = aaaa)
			muon_i = muon_indices[0] + int((data.shape[0] - remove_from_hdul[k_hdu].shape[0])/2)
			muon_j = muon_indices[1] + int((data.shape[1] - remove_from_hdul[k_hdu].shape[1])/2)
			data[
				 muon_i - int((mask.shape[0]-1)/2) : muon_i + int((mask.shape[0]-1)/2)+1, 
				 muon_j - int((mask.shape[1]-1)/2) : muon_j + int((mask.shape[1]-1)/2)+1
				] *= mask
			data[
				 muon_i - int((mask.shape[0]-1)/2) : muon_i + int((mask.shape[0]-1)/2)+1, 
				 muon_j - int((mask.shape[1]-1)/2) : muon_j + int((mask.shape[1]-1)/2)+1
				] += (1-mask)*(np.pi if args.replace_with in ['nan', 'NaN'] else float(args.replace_with))
		if args.replace_with.lower() in ['nan', 'inf']:
			data[data==np.pi] = float(args.replace_with)
		remove_from_hdul[k_hdu].data = data[
											 2*mask.shape[0] : 2*mask.shape[0] + remove_from_hdul[k_hdu].data.shape[0],
											 2*mask.shape[1] : 2*mask.shape[1] + remove_from_hdul[k_hdu].data.shape[1],
										   ]
	remove_from_hdul.writeto(remove_from_path + remove_from_fname[:-5] + '-Muons_removed.fits')
