import os
import ltapy
from timestamp import *
import numpy as np
from bureaucracy import *

PSAMP_VALS = [int(i) for i in np.logspace(np.log10(20), np.log10(1000), 5)]
NROW = 2100
EXPOSURE_TIME = 30*60 # seconds

PSAMP_CLK = 15e6
DELAY_INTEG_WIDTH_CLK = 100e6
DEAD_TIME = 4000/DELAY_INTEG_WIDTH_CLK - 189/PSAMP_CLK # Dead time in seconds

reading_dir = get_reading_dir('reading_time_sweep')

lta = ltapy.lta(reading_directory=reading_dir)

lta.erase_and_purge()

for psamp in PSAMP_VALS:
	print('Cleaning the CCD...')
	lta.do('NROW 2100')
	lta.do('NCOL 500')
	lta.read(reading_name='garbage')
	
	delay_Integ_Width = int((DEAD_TIME + psamp/PSAMP_CLK)*DELAY_INTEG_WIDTH_CLK)
	lta.do('NROW ' + str(NROW))
	lta.do('set psamp ' + str(psamp))
	lta.do('set ssamp ' + str(psamp))
	lta.do('delay_Integ_Width ' + str(delay_Integ_Width))
	print('Exposing CCD for ' + str(EXPOSURE_TIME) + ' seconds...')
	sleep(EXPOSURE_TIME)
	print('Reading with psamp = ' + str(psamp) + ' and delay_Integ_Width = ' + str(delay_Integ_Width))
	lta.read(reading_name='delay=' + str(delay_Integ_Width) + 'psamp=' + str(psamp))

lta.do('NROW 2100')
lta.do('NCOL 500')
