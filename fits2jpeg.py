import os
import sys
import argparse

parser = argparse.ArgumentParser(description='Generates "jpeg" files from the quadrants of a FITS file using the "histogram" scale of the program "ds9".')
parser.add_argument('--quadrant',
					help = 'The quadrant number you want to export. Default is all.',
					dest = 'quadrant',
					type = int,
					default = -1
					)
parser.add_argument('-i',
					metavar = 'fileORdirectory', 
					help = 'Path to a FITS file or a directory containing FITS files to process.',
					required = True,
					dest = 'file_or_directory'
					)

args = parser.parse_args()

directory_or_file = args.file_or_directory

def export_file(filepath):
	if args.quadrant == -1:
		quadrants = [0,1,2,3]
	else:
		quadrants = [args.quadrant]
	for k in quadrants:
			os.system('ds9 -fits ' + filepath + '[' + str(k) + '] -scale histequ -export ' + filepath[:-5] + '_' + str(k) + '.jpeg 10 -exit')

if '.fits' in directory_or_file: # In this case this is just a file
	export_file(directory_or_file)
else: # We have to process all the directory
	if directory_or_file[-1] != '/':
		directory_or_file += '/'
	for file in os.listdir(directory_or_file):
		if '.fits' in file:
			export_file(directory_or_file + file)
