# Usage example:
#
#		from time import sleep
#		import lakeshore
#
#		lakeshore.ctrl.quED_laser('on')
#		sleep(2)
#		lakeshore.ctrl.quED_laser('off')
#		sleep(2)
#		lakeshore.ctrl.toogle_quED_laser()
#		sleep(2)
#		# In this point the laser will be turned off automatically by the
#		# lakeshore module.
#
########################################################################

from time import sleep
import serial

class __lakeshore:
	def __init__(self, port='/dev/ttyUSB0'):
		self.port = port
		self.send_cmd('RELAY 1 0')
		self.send_cmd('RELAY 2 0')
		self.disable_laser_security_autopoweroff = False
	
	def send_cmd(self, cmd):
		ser = serial.Serial(
							port = self.port,
							baudrate = 9600,
							parity = serial.PARITY_ODD,
							stopbits = serial.STOPBITS_ONE,
							bytesize = serial.SEVENBITS
		)
		ser.write((cmd + '\n\r').encode())
		sleep(1)
		response = ''
		while ser.inWaiting() > 0:
			response += ser.read(1).decode('utf-8')
		ser.close()
		return response
	
	def laser_status(self):
		while True:
			response = self.send_cmd('KRDG? B')
			if response != '':
				break
		# ~ print('laser_status received from Lakeshore: ' + response) # Uncomment this for debugging
		temperature = float(response[1:-2])
		if temperature < 1 or temperature > 150:
			return 'off'
		if temperature > 1 and temperature < 150:
			return 'on'
		else:
			raise ValueError('Cannot translate temperature to laser status. Temperature received was: ' + str(temperature))
	
	def toogle_quED_laser(self):
		status = self.laser_status()
		while True:
			self.send_cmd('RELAY 1 1')
			self.send_cmd('RELAY 1 0')
			if status != self.laser_status():
				break
	
	def quED_laser(self, status):
		if status == 'on':
			while self.laser_status() == 'off':
				self.toogle_quED_laser()
		if status == 'off':
			while self.laser_status() == 'on':
				self.toogle_quED_laser()
	
	def __del__(self):
		if self.disable_laser_security_autopoweroff is True:
			print('WARNING: Finishing Lakeshore routine: Security auto power off of the laser has been disabled. Be sure that the laser is not on!')
		else:
			# This is to avoid forgetting the laser on after running a script, or if an error occurs.
			print('Finishing Lakeshore routine: Turning laser off, please wait...')
			sleep(2)
			while self.laser_status() == 'on':
				self.quED_laser('off')
			print('Laser status is ' + self.laser_status())
		print('Finishing Lakeshore routine: Turning LED off...')
		self.LED(status = 'off')
		print('LED is off')
	
	def analog_output(self,voltage = 0):
		if voltage > 10 or voltage < 0:
			raise ValueError('Voltage has to be between 0 and 10 V')
		while True:
			status = self.send_cmd("AOUT? ")
			while status == '':
				status = self.send_cmd("AOUT? ")
			if float(status)/10.0 > voltage - 0.1 and float(status)/10.0 < voltage + 0.1:
				break
			self.send_cmd("ANALOG 0,2,A,1,0,0," + str(voltage*10.0))
	
	def LED(self, status = None, voltage = None):
		if status != None and voltage != None:
			raise ValueError('"status" and "voltage" cannot be specified both at the same time. Tell me the status or the voltage, just one of them!')
		if voltage != None:
			if voltage > 5:
				raise ValueError('The maximum voltage for the LED is 5 V')
			self.analog_output(voltage)
		if status != None:
			if status == 'off':
				self.analog_output(0)
			elif status == 'on':
				self.analog_output(5)
			else:
				raise ValueError('Dont understand status = ' + str(status))
		
ctrl = __lakeshore()

if __name__ == '__main__':
	i = 0
	while True:
		ctrl.analog_output(i)
		i += 1
		if i > 10:
			i = 0

	
