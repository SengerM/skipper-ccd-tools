#!/usr/bin/perl -w

# File list.
$file_list = $ARGV[0];

if ($file_list eq "help")
{
	print "This script is used to extract the Gain and Noise from the images.\n";
	print "The file list can be created with the command:\n";
	print "  \$ ls NoiseScan*fits > file_list.txt\n";
	print "\n";
	print "Usage: $0 file_list.txt\n";
	exit(0);
}

open(my $f,'<', $file_list) or die "Could not open file '$file_list' $!";
@files = <$f>;
close($f);

%hash = ();
foreach $file (@files)
{
	chomp($file);
	if ($file =~ m/PINIT(\d+)_SINIT(\d+)_NSAMP(\d+)_\d+_(\d+)/)
	{
		# Extract samples and channel from file name.
		my $pinit = $1;
		my $sinit = $2;
		my $nsamp = $3;
		my $ch = $4;

		# Compute STD from image.
		my $out = `/home/ccds/Soft/noisamp/noisamp.exe $file -p -q [373:398,427:617]`;
		my @tmp = split(/\s+/, $out);
		my $std = $tmp[1];

		# Save std value into hash.
		$hash{$pinit}{$sinit}{$nsamp}{$ch}{std} = $std;

		# Build root file from fits using extract.exe.
		$cmd = "/home/ccds/Soft/extract/extract.exe $file -o img.root -w -n -c /home/ccds/Soft/extract/extractConfig.xml";
		system($cmd);

		# Compute the gain.
		$out = `/home/ccds/Soft/gain/gain_log.exe -i img.root -o $file -f \"E1\" -g 1 -e 5.9 -w 0.003745 -s $nsamp`;
		if ($out =~ m/gain\[0\]=\s+(.+)/)
		{
			# Save gain into hash.
			$hash{$pinit}{$sinit}{$nsamp}{$ch}{gain} = $1;
		}
		else 
		{
			$hash{$pinit}{$sinit}{$nsamp}{$ch}{gain} = "NaN";
		}

		
		# Remove temp files.	
		$cmd = "rm img.root";
		system($cmd);
	}
}

# Print table on output file.
open(my $f,'>', "scan.csv") or die "Could not open file 'scan.csv' $!";
print $f "PINIT,";
print $f "SINIT,";
print $f "NSAMP,";
print $f "CH0 mad,CH0 gain,";
print $f "CH1 mad,CH1 gain,";
print $f "CH2 mad,CH2 gain,";
print $f "CH3 mad,CH3 gain,";
print $f "\n";
@pinits = sort {$a <=> $b} keys %hash;
foreach $pinit (@pinits)
{
	@sinits = sort {$a <=> $b} keys %{$hash{$pinit}};
	foreach $sinit (@sinits)
	{
		@nsamps = sort {$a <=> $b} keys %{$hash{$pinit}{$sinit}};
		foreach $nsamp (@nsamps)
		{
			print $f "$pinit,$sinit,$nsamp,";
			@chs = sort {$a <=> $b} keys %{$hash{$pinit}{$sinit}{$nsamp}};
			foreach $ch (@chs)
			{
				my $std = $hash{$pinit}{$sinit}{$nsamp}{$ch}{std};
				my $gain = $hash{$pinit}{$sinit}{$nsamp}{$ch}{gain};
				
				print $f "$std,$gain,";
			}
			print $f "\n";
		}
	}
}
close($f);
