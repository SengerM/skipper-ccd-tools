#!/bin/bash

# Run this as ```source init_tools.sh```

chmod 755 $PWD/viewfits.sh
alias viewfits="$PWD/viewfits.sh"

alias fits2jpeg="python3 $PWD/fits2jpeg.py"
alias skipper2root="python3 $PWD/skipper2root.py"
alias combinequadrants="python3 $PWD/combinequadrants.py"
