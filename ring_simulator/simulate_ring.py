from darmat.SPDC import SPDC
import numpy as np
from astropy.io import fits
import matplotlib.pyplot as plt
import argparse
import os

parser = argparse.ArgumentParser(description='Simulates data taken with a CCD of an SPDC experiment.')
parser.add_argument('--pairs',
					metavar = 'N', 
					help = 'Number of events (pairs of signal+idler) to simulate.',
					dest = 'n_pairs',
					required = True,
					type = float
					)
parser.add_argument('--pixels',
					metavar = 'N', 
					help = 'Two integer numbers being the pixels in "x" direction and in "y" direction respectively. Default is "--pixels 2000 2000".',
					dest = 'img_pixels',
					nargs = 2,
					default = (2000,2000),
					type = int
					)
parser.add_argument('--center',
					metavar = 'N', 
					help = 'Two integer numbers being the "x" and "y" coordinates for the center of the ring in pixels. Default is "--center 1000 1000".',
					dest = 'center_pixels',
					nargs = 2,
					default = (1000,1000),
					type = int
					)
parser.add_argument('--pixel-size',
					metavar = 'size', 
					help = 'Size of the pixel in meters. Default is 15e-6.',
					dest = 'pixel_size',
					type = float,
					default = 15e-6
					)
parser.add_argument('--distance',
					metavar = 'd', 
					help = 'Distance between the BBO and the detector in meters. Default is 10e-2.',
					dest = 'd',
					type = float,
					default = 10e-2
					)
parser.add_argument('--lambda-pump',
					metavar = 'l', 
					help = 'Pump wavelength in meters. Default is 405e-9.',
					dest = 'lambda_pump',
					type = float,
					default = 405e-9
					)
parser.add_argument('--crystal-length',
					metavar = 'l', 
					help = 'Length of the nonlinear medium in the pump direction in meters. Default is 80e-6.',
					dest = 'crystal_l',
					type = float,
					default = 80e-6
					)
parser.add_argument('--n-pump',
					metavar = 'n', 
					help = 'Refractive index of the pump. Default is ' + str(1.67*np.cos(3*np.pi/180)) + '.',
					dest = 'n_pump',
					type = float,
					default = 1.67*np.cos(3*np.pi/180)
					)
parser.add_argument('--n-signal',
					metavar = 'n', 
					help = 'Refractive index of the signal. Default is 1.67.',
					dest = 'n_signal',
					type = float,
					default = 1.67
					)
parser.add_argument('--n-idler',
					metavar = 'n', 
					help = 'Refractive index of the idler. Default is 1.67.',
					dest = 'n_idler',
					type = float,
					default = 1.67
					)
parser.add_argument('--alpha',
					metavar = 'a', 
					help = 'Value of the alpha coefficient, which is the ratio of the energy of the signal over the energy of the pump. Default is 0.5.',
					dest = 'alpha',
					type = float,
					default = .5
					)
parser.add_argument('--max-samples-in-ram',
					metavar = 'N', 
					help = 'Maximum number of samples to store simultaneously in RAM. Default is 9999.',
					dest = 'max_samples_in_ram',
					type = int,
					default = 9999
					)
parser.add_argument('--odir',
					metavar = 'fname',
					help = 'Name (or path) of directory for saving the output. Default is "simulation" in current working directory.',
					dest = 'odir',
					default = 'simulation'
					)

args = parser.parse_args()

odir = args.odir + ('/' if args.odir[-1] != '/' else '')
os.mkdir(odir)

DISTANCE_BBO_CCD = args.d
RING_CENTER_PIXELS = args.center_pixels
image_pixels = args.img_pixels
N_PAIRS = int(args.n_pairs)
PIXEL_SIZE = args.pixel_size
MAX_SIMULTANEOUS_SAMPLES = args.max_samples_in_ram

spdc = SPDC( # Using parameters of current experiment
	lambda_pump = args.lambda_pump, 
	crystal_l = args.crystal_l, 
	n_pump = args.n_pump, 
	n_signal = args.n_signal, 
	n_idler = args.n_idler, 
	alpha = args.alpha
)

# Plot expected intensity ----------------------------------------------

x, y = np.meshgrid(
	np.linspace(0,image_pixels[0]*PIXEL_SIZE,image_pixels[0]),
	np.linspace(0,image_pixels[1]*PIXEL_SIZE,image_pixels[1]),
)
r_matrix = ((x-RING_CENTER_PIXELS[0]*PIXEL_SIZE)**2 + (y-RING_CENTER_PIXELS[1]*PIXEL_SIZE)**2)**.5
theta_matrix = np.arctan(r_matrix/DISTANCE_BBO_CCD)

_, intensity = spdc.signal_intensity(theta_matrix)

fig, ax = plt.subplots()
ax.imshow(intensity)
fig.savefig(odir + 'intensity_profile.png')

hdul_new = fits.PrimaryHDU(intensity)
hdul_new.writeto(odir + 'intensity_profile.fits')

# Simulation -----------------------------------------------------------

def simulate_picture(number_of_pairs):
	phi_signal_samples, theta_signal_samples = spdc.signal_samples(number_of_pairs)
	theta_signal_samples = np.array(theta_signal_samples)
	theta_idler_samples = spdc.theta_idler(theta_signal_samples)
	phi_idler_samples = phi_signal_samples + np.pi

	photons_r = np.concatenate((np.tan(theta_signal_samples),np.tan(theta_idler_samples)))*DISTANCE_BBO_CCD
	photons_phi = np.concatenate((phi_signal_samples,phi_idler_samples))

	photons_x = photons_r*np.cos(photons_phi) + RING_CENTER_PIXELS[0]*PIXEL_SIZE
	photons_y = photons_r*np.sin(photons_phi) + RING_CENTER_PIXELS[1]*PIXEL_SIZE

	simulation, x_edges, y_edges = np.histogram2d(
		x = photons_x/PIXEL_SIZE,
		y = photons_y/PIXEL_SIZE,
		bins = image_pixels,
		range = [
				   [0, image_pixels[0]],
				   [0, image_pixels[1]]
				]
	)
	return simulation

simulation = simulate_picture(N_PAIRS%MAX_SIMULTANEOUS_SAMPLES)

hdul_new = fits.PrimaryHDU(simulation)
hdul_new.writeto(odir + 'simulation.fits')
with open(odir + 'log.txt', 'w') as log_file:
	print('Simulated events: ' + str(int(simulation.sum()/2)), file = log_file)

number_of_rounds = int(np.floor(N_PAIRS/MAX_SIMULTANEOUS_SAMPLES))
for round_number in range(number_of_rounds):
	simulation += simulate_picture(MAX_SIMULTANEOUS_SAMPLES)
	hdul_new.data = simulation
	os.system('rm ' + odir + 'simulation.fits')
	hdul_new.writeto(odir + 'simulation.fits')
	with open(odir + 'log.txt', 'w') as log_file:
		print('Simulated events: ' + str(int(simulation.sum()/2)), file = log_file)
	string = 'Simulation progress: ' + str((round_number+1)*MAX_SIMULTANEOUS_SAMPLES + N_PAIRS%MAX_SIMULTANEOUS_SAMPLES) + '/' + str(N_PAIRS) + ' samples simulated'
	print(string, end = '\r'*len(string))
if number_of_rounds > 0:
	print('')

fig, ax = plt.subplots()
ax.imshow(simulation)
fig.savefig(odir + 'simulation.png')

# ~ hdul_new = fits.PrimaryHDU(simulation)
# ~ hdul_new.writeto(odir + 'simulation.fits')
