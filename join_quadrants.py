import os
import sys
from astropy.io import fits
import numpy as np
import matplotlib.pyplot as plt

def map_quadrant(k):
	if k == 0: return 0, 1
	if k == 1: return 1, 1
	if k == 2: return 0, 0
	if k == 3: return 1, 0
	raise ValueError('Not a valid quadrant number')

def transform_quadrant(k, data):
	data = np.array(data)
	data = data.transpose()
	if k == 0: return np.flip(data, 1)
	if k == 1: return np.flip(np.flip(data, 0), 1)
	if k == 2: return data
	if k == 3: return np.flip(data, 0)

def process_file(path, file):
	print('Joining quadrants of file ' + "'" + path + file + "'")
	with fits.open(path + file) as hdul:
		fig, ax = plt.subplots(2,2)
		active_area = []
		for k in range(4):
			i,j = map_quadrant(k)
			ax[i][j].imshow(transform_quadrant(k, hdul[k].data))
			CCDNPRES = int(hdul[k].header['CCDNPRES'])
			CCDNCOL = int(hdul[k].header['CCDNCOL'])
			CCDNROW = int(hdul[k].header['CCDNROW'])
			# ~ active_area.append(transform_quadrant(k, [row[CCDNPRES:CCDNPRES+int(CCDNCOL)] for row in hdul[k].data[0:int(CCDNROW/2)]]))
			active_area.append(hdul[k].data)
		left_data = list(active_area[2]) + list(active_area[3])
		right_data = list(active_area[0]) + list(active_area[1])
		new_data = [list(left)+list(right) for left,right in zip(left_data,right_data)]
		fig, ax = plt.subplots()
		ax.imshow(active_area[0])
		new_hdul = fits.PrimaryHDU(active_area[0])
		new_hdul.header = hdul[0].header
		new_hdul.header['NCOL'] = str(int(CCDNCOL))
		new_hdul.header['NROW'] = str(int(CCDNROW/2))
		new_hdul.header['CCDNPRES'] = str(0)
		new_hdul.writeto(path + file[:-5] + '-Active_area_joint.fits')
		
	# ~ plt.show()
		
########################################################################

directory_or_file = sys.argv[1]		
if '.fits' in directory_or_file: # In this case this is just a file
	process_file(
		path = directory_or_file[:directory_or_file.rfind('/')+1],
		file = directory_or_file[directory_or_file.rfind('/')+1:]
		)
else: # We have to process all the directory
	if directory_or_file[-1] != '/':
		directory_or_file += '/'
	for file in os.listdir(directory_or_file):
		if '.fits' in file:
			process_file(path = directory_or_file, file = file)
