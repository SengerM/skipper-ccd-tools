ALPHA_DEFAULT = 0.00198975
BETA_DEFAULT = 8.84892e-11
GAMMA_DEFAULT = -6.86283e-17
DELTA_DEFAULT = 3.10919e-23

import os
import sys
from astropy.io import fits
import numpy as np
import argparse

########################################################################

parser = argparse.ArgumentParser(description='Subtract the overscan from CCD images.')
parser.add_argument('-i',
	metavar = 'fileORdirectory', 
	help = 'Path to a FITS file or a directory containing FITS files to process. It is assumed that the FITS files header has the format from the LTA daemon.',
	required = True,
	dest = 'file_or_directory'
)
parser.add_argument('--alpha',
	help = 'The value of the alpha parameter. Default is ' + str(ALPHA_DEFAULT) + '.',
	dest = 'alpha',
	default = ALPHA_DEFAULT
)
parser.add_argument('--beta',
	help = 'The value of the beta parameter. Default is ' + str(BETA_DEFAULT) + '.',
	dest = 'beta',
	default = BETA_DEFAULT
)
parser.add_argument('--gamma',
	help = 'The value of the gamma parameter. Default is ' + str(GAMMA_DEFAULT) + '.',
	dest = 'gamma',
	default = GAMMA_DEFAULT
)
parser.add_argument('--delta',
	help = 'The value of the delta parameter. Default is ' + str(DELTA_DEFAULT) + '.',
	dest = 'delta',
	default = DELTA_DEFAULT
)


args = parser.parse_args()

def ADU2e(adu):
	return np.round(args.alpha*adu + args.beta*adu**2 + args.gamma*adu**3 + args.delta*adu**4)

def process_file(directory, file, args):
	with fits.open(directory + file) as hdulist:
		for k in range(len(hdulist)):
			hdulist[k].data = ADU2e(hdulist[k].data)
		hdulist.writeto(directory + file[:-5] + '-ADU2e' + '.fits')

directory_or_file = args.file_or_directory

if '.fits' in directory_or_file: # In this case this is just a file
	process_file(
		directory = directory_or_file[:directory_or_file.rfind('/')+1],
		file = directory_or_file[directory_or_file.rfind('/')+1:],
		args = args
		)
else: # We have to process all the directory
	if directory_or_file[-1] != '/':
		directory_or_file += '/'
	for file in os.listdir(directory_or_file):
		if '.fits' in file:
			process_file(directory = directory_or_file, file = file, args = args)
