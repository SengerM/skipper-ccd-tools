import os
import sys
from astropy.io import fits
import numpy as np
import argparse

def process_file(directory, file, args):
	with fits.open(directory + file) as hdulist:
		for k in range(len(hdulist)):
			CCDNPRES = int(hdulist[k].header['CCDNPRES'])
			CCDNCOL = int(hdulist[k].header['CCDNCOL'])
			CCDNROW = int(hdulist[k].header['CCDNROW'])
			NCOL = int(hdulist[k].header['NCOL'])
			NROW = int(hdulist[k].header['NROW'])
			overscan_mean = hdulist[k].data[:,CCDNPRES+int(CCDNCOL)+1:].mean(1) # Replace "CCDNCOL" by "CCDNCOL/2" if you are reading through the 4 quadrants.
			
			if args.remove_overscan == True:
				hdulist[k].data = hdulist[k].data[:int(CCDNROW/2), CCDNPRES:CCDNPRES+int(CCDNCOL)]
			
			for k_row in range(hdulist[k].data.shape[0]):
				hdulist[k].data[k_row] -= overscan_mean[k_row]
			
		hdulist.writeto(directory + file[:-5] + '-Overscan_subtracted' + ('_and_removed' if args.remove_overscan == True else '') + '.fits')

########################################################################

parser = argparse.ArgumentParser(description='Subtract the overscan from CCD images.')
parser.add_argument('--remove-overscan',
					help = 'The overscan (and prescan) pixels are removed from the image. Only the active area is saved. (Default is to keep all.)',
					dest = 'remove_overscan',
					action = 'store_true',
					default = False
					)
parser.add_argument('-i',
					metavar = 'fileORdirectory', 
					help = 'Path to a FITS file or a directory containing FITS files to process. It is assumed that the FITS files header has the format from the LTA daemon.',
					required = True,
					dest = 'file_or_directory'
					)


args = parser.parse_args()

directory_or_file = args.file_or_directory

if '.fits' in directory_or_file: # In this case this is just a file
	process_file(
		directory = directory_or_file[:directory_or_file.rfind('/')+1],
		file = directory_or_file[directory_or_file.rfind('/')+1:],
		args = args
		)
else: # We have to process all the directory
	if directory_or_file[-1] != '/':
		directory_or_file += '/'
	for file in os.listdir(directory_or_file):
		if '.fits' in file:
			process_file(directory = directory_or_file, file = file, args = args)
