import os
import sys

SKIPPER2ROOT_PATH = '/home/lta-test/Desktop/DarMat/skipper2root/skipper2root.exe'

########################################################################
# 
# This scripts converts process '.fits' files using skipper2root. The 
# advantage of this scritp is that it process just one '.fits' file or
# all the '.fits' files in one directory, depending on the given argument.
# 
# Examples
# 
# python skipper2root.py /home/me/my_file.fits 
#
# The previous line process just one file.
#
# python skipper2root.py /home/me/directory_with_fits/ 
#
# The previous line process all the files in the directory.
# 
########################################################################

directory_or_file = sys.argv[1]

def skipper2root(path, file):
	print('Processing file ' + "'" + path + file + "'")
	os.system(SKIPPER2ROOT_PATH + ' -i ' + path + file + ' -o ' + path + file[:file.rfind('.')])

if '.fits' in directory_or_file: # In this case this is just a file
	skipper2root(
		path = directory_or_file[:directory_or_file.rfind('/')+1],
		file = directory_or_file[directory_or_file.rfind('/')+1:]
		)
else: # We have to process all the directory
	if directory_or_file[-1] != '/':
		directory_or_file += '/'
	for file in os.listdir(directory_or_file):
		if '.fits' in file:
			skipper2root(path = directory_or_file, file = file)
