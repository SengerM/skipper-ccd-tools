from datetime import datetime
import ltapy
from time import sleep
import threading
import os
import matplotlib.pyplot as plt
import numpy as np
from timestamp import get_timestamp
import csv

NOISE_LOG_FILE_PATH = '/home/lta-test/Desktop/DarMat/reads'
NOISE_LOG_PLOT_PATH = '/home/lta-test/Desktop/DarMat/reads/'

########################################################################

NOISE_LOG_FILE_PATH = NOISE_LOG_FILE_PATH + '/' if NOISE_LOG_FILE_PATH[-1] != '/' else NOISE_LOG_FILE_PATH
NOISE_LOG_PLOT_PATH = NOISE_LOG_PLOT_PATH + '/' if NOISE_LOG_PLOT_PATH[-1] != '/' else NOISE_LOG_PLOT_PATH

data = np.genfromtxt(
	NOISE_LOG_FILE_PATH + 'noise_log.txt',
	comments = '#').transpose()

quadrant = [None]*4
for k in range(4):
	quadrant[k] = data[1][data[0] == k+1]

with open(NOISE_LOG_FILE_PATH + 'noise_log.txt', 'r') as f:
	reader = csv.reader(f)
	data = list(reader)
timestamps = []
prev_timestamp_idx = 0
comments = []
for idx,line in enumerate(data):
	if idx == prev_timestamp_idx + 1: # This is an error, sometimes there are two consecutive timestamps. Maybe this happens when the daemon crashes, I don't know.
		continue
	if '#' in line[0]: # This means it is a timestamp
		timestamps.append(datetime.strptime(line[0][2:14], '%y%m%d%H%M%S'))
		prev_timestamp_idx = idx
	if len(line[0]) > 14: # This means that there is a comment
		comments.append({'timestamp': timestamps[-1], 'text': line[0][16:-1]})

fig, ax = plt.subplots()
for k in range(4):
	ax.plot(
		timestamps,
		quadrant[k],
		label = 'quadrant ' + str(k+1),
		marker = '.'
		)
for comment in comments:
	y_pos = []
	for k in range(4):
		y_pos.append(quadrant[k][timestamps.index(comment.get('timestamp'))])
	y_pos = max(y_pos)
	ax.text(
		x = comment.get('timestamp'),
		y = y_pos,
		s = comment.get('text')
		)
ax.legend()
ax.set_yscale('log')
ax.grid(which = 'both', b = True)
labels = ax.get_xticklabels()
plt.setp(labels, rotation=20, horizontalalignment='right')
plt.show()
