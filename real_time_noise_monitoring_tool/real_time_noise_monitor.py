from datetime import datetime
import ltapy
from time import sleep
import threading
import os
import matplotlib.pyplot as plt
import numpy as np
from timestamp import get_timestamp
import csv
from paths import *

ACTIVE_AREA_COLS = 886
COL_OVERSCAN = 500
COL_PRESCAN = 7
NROWS = 50


print('Starting script!')

lta = ltapy.lta(reading_directory=TEMP_STORAGE_DIR_PATH)

def read():
	while True:
		print('Reading...')
		lta.read(
			timestamp = False,
			reading_name = 'scan',
			NCOL = COL_PRESCAN + int(ACTIVE_AREA_COLS/2) + COL_OVERSCAN,
			NROW = NROWS
			)
		print('Read done!')
		os.mkdir(TEMP_STORAGE_DIR_PATH + 'reading_done')

def process():
	while True:
		while 'reading_done' not in os.listdir(TEMP_STORAGE_DIR_PATH):
			sleep(1)
		print('Processing data...')
		os.system('rm -r ' + TEMP_STORAGE_DIR_PATH + 'reading_done')
		os.system('echo \# ' + get_timestamp() + '>> ' + NOISE_LOG_FILE_PATH + 'noise_log.txt')
		os.system(NOISAMP_EXE_PATH + 'noisamp.exe' + ' ' + TEMP_STORAGE_DIR_PATH + 'scan.fits -p -q [' + str(COL_PRESCAN + int(ACTIVE_AREA_COLS/2)) + ':' + str(COL_PRESCAN + int(ACTIVE_AREA_COLS/2) + COL_OVERSCAN - 5) + ',1:' + str(NROWS) + '] >> ' + NOISE_LOG_FILE_PATH + 'noise_log.txt')
		os.mkdir(TEMP_STORAGE_DIR_PATH + 'new_data_for_plotting')
		print('Processing done!')

def plot():
	while True:
		while 'new_data_for_plotting' not in os.listdir(TEMP_STORAGE_DIR_PATH):
			sleep(1)
		print('Plotting data...')
		os.system('rm -r ' + TEMP_STORAGE_DIR_PATH + 'new_data_for_plotting')
		os.system('python3 ' + PLOTTING_SCRIPT_PATH)
		print('Plotting done!')

reading_thread = threading.Thread(target=read, name='reading')
processing_thread = threading.Thread(target=process, name='processing')
plotting_thread = threading.Thread(target=plot, name='plotting')

reading_thread.start()
processing_thread.start()
plotting_thread.start()

while True:
	sleep(1)
