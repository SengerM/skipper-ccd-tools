NOISE_LOG_FILE_PATH = '/home/darmat/lta_reads'
NOISE_LOG_PLOT_PATH = '/home/darmat/lta_reads'
NOISAMP_EXE_PATH = '/home/darmat/soft/skipper-ccd-tools/noisamp/noisamp.exe'
TEMP_STORAGE_DIR_PATH = '/home/darmat/lta_reads/garbage'
PLOTTING_SCRIPT_PATH = '/home/darmat/soft/skipper-ccd-tools/real_time_noise_monitoring_tool/plot.py'

########################################################################

NOISE_LOG_FILE_PATH = NOISE_LOG_FILE_PATH + '/' if NOISE_LOG_FILE_PATH[-1] != '/' else NOISE_LOG_FILE_PATH
NOISE_LOG_PLOT_PATH = NOISE_LOG_PLOT_PATH + '/' if NOISE_LOG_PLOT_PATH[-1] != '/' else NOISE_LOG_PLOT_PATH
NOISAMP_EXE_PATH = NOISAMP_EXE_PATH + '/' if NOISAMP_EXE_PATH[-1] != '/' else NOISAMP_EXE_PATH
TEMP_STORAGE_DIR_PATH = TEMP_STORAGE_DIR_PATH + '/' if TEMP_STORAGE_DIR_PATH[-1] != '/' else TEMP_STORAGE_DIR_PATH
