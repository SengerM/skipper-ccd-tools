from datetime import datetime
import ltapy
from time import sleep
import threading
import os
import matplotlib.pyplot as plt
import numpy as np
from timestamp import get_timestamp
import csv
from paths import *

N_SAMPLES_TO_PLOT = 120

data = np.genfromtxt(
	NOISE_LOG_FILE_PATH + 'noise_log.txt',
	comments = '#').transpose()

quadrant = [None]*4
for k in range(4):
	quadrant[k] = data[1][data[0] == k+1]

with open(NOISE_LOG_FILE_PATH + 'noise_log.txt', 'r') as f:
	reader = csv.reader(f)
	data = list(reader)
timestamps = []
comments = []
prev_timestamp_idx = 0
for idx,line in enumerate(data):
	if idx == prev_timestamp_idx + 1: # This is an error, sometimes there are two consecutive timestamps. Maybe this happens when the daemon crashes, I don't know.
		continue
	if '#' in line[0]: # This means it is a timestamp
		timestamps.append(datetime.strptime(line[0][2:14], '%y%m%d%H%M%S'))
		prev_timestamp_idx = idx
	if len(line[0]) > 14: # This means that there is a comment
		comments.append({'timestamp': timestamps[-1], 'text': line[0][16:-1]})

fig, ax = plt.subplots()
for k in range(4):
	ax.plot(
		timestamps[-N_SAMPLES_TO_PLOT:],
		quadrant[k][-N_SAMPLES_TO_PLOT:],
		label = 'quadrant ' + str(k+1),
		marker = '.'
		)
for comment in comments:
	y_pos = []
	for k in range(4):
		y_pos.append(quadrant[k][timestamps.index(comment.get('timestamp'))])
	y_pos = max(y_pos)
	ax.text(
		x = comment.get('timestamp'),
		y = y_pos,
		s = comment.get('text')
		)
ax.set_ylabel('Overscan mean value')
ax.set_yscale('log')
ax.legend()
ax.grid(which = 'both', b = True, color = (.9,.9,.9))
labels = ax.get_xticklabels()
plt.setp(labels, rotation=20, horizontalalignment='right')
fig.savefig(NOISE_LOG_PLOT_PATH + 'noise_log.png')
plt.close(fig)
