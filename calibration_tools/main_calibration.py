import os
import sys
from paths_and_filenames import *

directory_or_file = sys.argv[1]

def process_file(path, file):
	print('Starting main calibration of file ' + "'" + path + file + "'")
	os.mkdir(path + file[:-5] + CALIBRATION_DIR_NAME_SUFFIX)
	os.system('python3 ltafits2skippertoroot.py ' + path + file)
	os.system('mv ' + path + 'proc_' + file + ' ' + path + file[:-5] + CALIBRATION_DIR_NAME_SUFFIX + '/' + file[:-5] + SKIPPER2ROOT_CALIBRATION_FILE_SUFFIX + '.fits')
	os.system('python3 non_binned_calibration.py ' + path + file[:-5] + CALIBRATION_DIR_NAME_SUFFIX + '/' + file[:-5] + SKIPPER2ROOT_CALIBRATION_FILE_SUFFIX + '.fits')
	os.system('python3 skippertoroot2electronsscale.py ' + path + file[:-5] + CALIBRATION_DIR_NAME_SUFFIX + '/' + file[:-5] + SKIPPER2ROOT_CALIBRATION_FILE_SUFFIX + '.fits')
	os.system('mv ' + path + file[:-5] + CALIBRATION_DIR_NAME_SUFFIX + '/' + file[:-5] + SKIPPER2ROOT_CALIBRATION_FILE_SUFFIX + ELECTRONS_SCALE_FILE_SUFFIX + '.fits' + ' ' + path + file[:-5] + ELECTRONS_SCALE_FILE_SUFFIX + '.fits')
	print('Main calibration of file ' + path + file + ' completed.')
	print('Created file ' + path + file[:-5] + ELECTRONS_SCALE_FILE_SUFFIX + '.fits')

if '.fits' in directory_or_file: # In this case this is just a file
	process_file(
		path = directory_or_file[:directory_or_file.rfind('/')+1],
		file = directory_or_file[directory_or_file.rfind('/')+1:]
		)
else: # We have to process all the directory
	if directory_or_file[-1] != '/':
		directory_or_file += '/'
	for file in os.listdir(directory_or_file):
		if '.fits' in file:
			process_file(path = directory_or_file, file = file)
