from numbers import Number

def compute_ecdf(data):
	"""
	Given a list of numbers in <data> this function computes the 
	empirical cumulated density function (ECDF).
	Returns
	-------
	x: List containing the x values for the ECDF.
	ecdf: List containing the ECDF value for each x.
	"""
	data = sorted(data)
	x = [data[0]]
	ecdf = [1]
	for dat in data[1:]:
		if x[-1] == dat:
			ecdf[-1] += 1
		else:
			x.append(dat)
			ecdf.append(ecdf[-1])
			ecdf[-1] += 1
	return x, [i/len(data) for i in ecdf]

def get_ecdf_in(x, x_ecdf, ecdf_data):
	if isinstance(x, Number):
		if x < x_ecdf[0]:
			return 0
		for k in range(len(x_ecdf)):
			if x < x_ecdf[k]:
				return ecdf_data[k-1]
		if x > x_ecdf[-1]:
			return 1
	else:
		return_list = []
		for xx in x:
			return_list.append(get_ecdf_in(xx, x_ecdf, ecdf_data))
		return return_list
