# ~ def process_file(filepath):
	# ~ print('Processing ' + filepath)
	# ~ if filepath[filepath.rfind('/')+1:filepath.rfind('.')] + CALIBRATION_RESULTS_DIR_NAME_SUFFIX not in os.listdir(filepath[:filepath.rfind('/')]):
		# ~ raise ValueError('Cannot process file because there is no calibration. Maybe you first have to run the calibration script...')
	# ~ calibration_params = np.genfromtxt(fname = filepath[:filepath.rfind('.')] + CALIBRATION_RESULTS_DIR_NAME_SUFFIX + '/' + CALIBRATION_PARAMS_FILE_NAME, skip_header = 1)
	# ~ with fits.open(filepath) as hdul:
		# ~ for quadrant in [0,1,2,3]:
			# ~ hdul[quadrant].data = (hdul[quadrant].data/int(hdul[quadrant].header['PSAMP']) - calibration_params[quadrant][2])/calibration_params[quadrant][1]
		# ~ hdul.writeto((filepath.split('proc_')[0] + filepath.split('proc_')[1])[:-5] + '_electrons_scale.fits')

import os
import sys
from paths_and_filenames import *
from astropy.io import fits
import numpy as np

directory_or_file = sys.argv[1]

def process_file(path, file):
	print('Starting conversion of file ' + "'" + path + file + "' to electrons scale")
	if CALIBRATION_RESULTS_SUBDIR not in os.listdir(path):
		raise ValueError('Cannot convert file because there is no calibration directory')
	calibration_params = np.genfromtxt(fname = path + CALIBRATION_RESULTS_SUBDIR + '/' + CALIBRATION_PARAMS_ACTIVE_AREA_FILE_NAME, skip_header = 1)
	with fits.open(path + file) as hdul:
		for quadrant in [0,1,2,3]:
			hdul[quadrant].data = (hdul[quadrant].data/int(hdul[quadrant].header['PSAMP']) - calibration_params[quadrant][2])/calibration_params[quadrant][1]
		hdul.writeto(path + file[:-5] + ELECTRONS_SCALE_FILE_SUFFIX + '.fits')

if '.fits' in directory_or_file: # In this case this is just a file
	process_file(
		path = directory_or_file[:directory_or_file.rfind('/')+1],
		file = directory_or_file[directory_or_file.rfind('/')+1:]
		)
else: # We have to process all the directory
	if directory_or_file[-1] != '/':
		directory_or_file += '/'
	for file in os.listdir(directory_or_file):
		if '.fits' in file:
			process_file(path = directory_or_file, file = file)
