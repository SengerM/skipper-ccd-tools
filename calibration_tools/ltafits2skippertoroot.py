import os
import sys
from paths_and_filenames import *
from astropy.io import fits

directory_or_file = sys.argv[1]

def skipper2root(path, file):
	os.system(SKIPPER2ROOT_EXE_PATH + ' -i -a2.5 -n ' + path + file + ' -o ' + path + file[:file.rfind('.')])
	with fits.open(path + 'proc_' + file) as hdul:
		os.system('rm ' + path + 'proc_' + file)
		hdul.writeto(path + 'proc_' + file)

if '.fits' in directory_or_file: # In this case this is just a file
	skipper2root(
		path = directory_or_file[:directory_or_file.rfind('/')+1],
		file = directory_or_file[directory_or_file.rfind('/')+1:]
		)
else: # We have to process all the directory
	if directory_or_file[-1] != '/':
		directory_or_file += '/'
	for file in os.listdir(directory_or_file):
		if '.fits' in file:
			skipper2root(path = directory_or_file, file = file)
