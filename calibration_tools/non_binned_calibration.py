MAX_ELECTRONS_CALIB = 5

########################################################################

from astropy.io import fits
import matplotlib.pyplot as plt
import numpy as np
from numbers import Number
from scipy.stats import norm
from scipy.stats import poisson
import lmfit
import os
import sys
from statistics_tools import compute_ecdf, get_ecdf_in
from paths_and_filenames import *

def map_quadrant(k):
	if k == 0: return 0, 1
	if k == 1: return 1, 1
	if k == 2: return 0, 0
	if k == 3: return 1, 0
	raise ValueError('Not a valid quadrant number')

def transform_quadrant(k, data):
	data = data.transpose()
	if k == 0: return np.flip(data, 1)
	if k == 1: return np.flip(np.flip(data, 0), 1)
	if k == 2: return data
	if k == 3: return np.flip(data, 0)

def model_poisson_forced(x, gain, shift, sigma, mu):
	p = poisson.pmf(range(MAX_ELECTRONS_CALIB+1), mu)
	cdf = np.zeros(len(x))
	for k in range(len(p)):
		cdf += p[k]*norm.cdf(x, loc = gain*k + shift, scale = sigma)
	return cdf

def model_free_amplitudes(x, gain, shift, sigma, *w):
	p = [i for i in w]
	cdf = np.zeros(len(x))
	for k in range(len(p)):
		cdf += p[k]*norm.cdf(x, loc = gain*k + shift, scale = sigma)
	return cdf

def process_file(filepath):
	print('Starting calibration of ' + filepath)
	calibration_results_dir = filepath[:filepath.rfind('/')] + '/' + CALIBRATION_RESULTS_SUBDIR + '/'
	os.mkdir(calibration_results_dir)
	print('Results will be saved in ' + calibration_results_dir)
	with open(calibration_results_dir + CALIBRATION_LOG_FILE_NAME, 'w') as log_file:
		print('# Calibration log of ' + filepath[filepath.rfind('/')+1:], file = log_file)
		print('', file = log_file)
		print('This is the calibration log for file ' + filepath[filepath.rfind('/')+1:] + '. The following calibration parameters were found fitting the "forced poisson amplitudes model" to the data after dividing the input FITS data by PSAMP. To be clear, the steps followed were:', file = log_file)
		print('', file = log_file)
		print('1. Read the file ' + filepath[filepath.rfind('/')+1:] + ' to obtain the data stored in the matrix ```data```.', file = log_file)
		print('2. Divide all the data by PSAMP in order to get an "aproximately electrons scale". This is ```new_data = data/PSAMP```.', file = log_file)
		print('3. Fit a sum of ' + str(MAX_ELECTRONS_CALIB) + ' gaussians\n\t- all with the same ```sigma```, \n\t- all separated by the same ```gain``` parameter, \n\t- all shifted by the same ```shift``` parameter and \n\t- the relative weights given by a poissonian distribution with parameter ```mu```\n\t to the ```new_data```.\n', file = log_file)
		print('', file = log_file)
		print('The **gain equation** relating the original units in file ' + filepath[filepath.rfind('/')+1:] + ' and the electrons units is\n\n\t original_units = (electron_units*gain + shift)*PSAMP\n\n where ```gain``` and ```shift``` are the values returned by this calibration and ```PSAMP``` is the value in the header of ' + filepath[filepath.rfind('/')+1:] + '.', file = log_file)
		print('', file = log_file)
	
	with open(calibration_results_dir + CALIBRATION_PARAMS_ACTIVE_AREA_FILE_NAME, 'w') as calib_params_file:
		print('Quadrant number\tGain (after dividing by PSAMP)\tShift (after dividing by PSAMP)\tSigma (after dividing by PSAMP)\tMu', file = calib_params_file)
	with open(calibration_results_dir + CALIBRATION_PARAMS_OVERSCAN_FILE_NAME, 'w') as calib_params_file:
		print('Quadrant number\tGain (after dividing by PSAMP)\tShift (after dividing by PSAMP)\tSigma (after dividing by PSAMP)\tMu', file = calib_params_file)
	
	model = lmfit.Model(model_poisson_forced)
	
	with fits.open(filepath) as hdu_list:
		active_area_data = []
		overscan_data = []
		x_vals_active_area = []
		ecdf_active_area = []
		params_active_area = []
		fit_result_active_area = []
		x_vals_overscan = []
		ecdf_overscan = []
		params_overscan = []
		fit_result_overscan = []
		for k in [0,1,2,3]: # "k" sweeps along each quadrant
			with open(calibration_results_dir + CALIBRATION_LOG_FILE_NAME, 'a') as log_file:
				print('## Quadrant number ' + str(k), file=log_file)
				print('', file = log_file)
			PSAMP = int(hdu_list[k].header['PSAMP'])
			NCOL = int(hdu_list[k].header['NCOL'])
			CCDNPRES = int(hdu_list[k].header['CCDNPRES'])
			CCDNCOL = int(hdu_list[k].header['CCDNCOL'])
			NSAMP = int(hdu_list[k].header['NSAMP'])
			active_area_data.append([row[CCDNPRES:CCDNPRES+int(CCDNCOL/2)] for row in hdu_list[k].data])
			active_area_data[k] = np.array(active_area_data[k]).ravel() # Convert it to a one dimensional numpy array
			active_area_data[k] /= PSAMP
			print('Mean active area: ' + str(active_area_data[k].mean()))
			print('Active area std: ' + str(active_area_data[k].std()))
			print('Max active area: ' + str(max(active_area_data[k])))
			print('Min active area: ' + str(min(active_area_data[k])))
			active_area_data[k] = [i for i in active_area_data[k] if i > -2 and i < MAX_ELECTRONS_CALIB*1.5] # Assuming that there is not a very big shift and that the gain is approximately 1 (after dividing by PSAMP). Also discarding all the data for high occupancy pixels.
			active_area_data[k] = np.array(active_area_data[k]).ravel() # Cast from list to a numpy array.
			x_, ecdf_ = compute_ecdf(active_area_data[k])
			x_vals_active_area.append(x_)
			ecdf_active_area.append(ecdf_)
			
			# Fit model to active area ---------------------------------
			print('Fitting model to active area of quadrant ' + str(k))
			params_active_area.append(model.make_params())
			params_active_area[k]['gain'].set(value = 1, max = 1.5, min = .5) # Initial guess
			params_active_area[k]['shift'].set(value = 0, max = 1, min = -1) # Initial guess
			params_active_area[k]['sigma'].set(value = NSAMP**-.5*4, min = 0) # Initial guess
			params_active_area[k]['mu'].set(value = active_area_data[k].ravel().std()*.9, min = 0) # Initial guess
			fit_result_active_area.append(model.fit(ecdf_active_area[k], params_active_area[k], x = x_vals_active_area[k]))
			
			with open(calibration_results_dir + CALIBRATION_LOG_FILE_NAME, 'a') as log_file:
				print('### Active area', file = log_file)
				print('', file = log_file)
				print('Gain equation params:', file = log_file)
				print('', file = log_file)
				print('- ```PSAMP``` = ' + str(PSAMP) + ' (from ' + filepath[filepath.rfind('/')+1:] + ' header)', file = log_file)
				print('- ```gain```: ' + str(fit_result_active_area[k].params['gain'].value) + ' (from fitting)', file = log_file)
				print('- ```shift```: ' + str(fit_result_active_area[k].params['shift'].value) + ' (from fitting)', file = log_file)
				print('', file = log_file)
				print('Fitting results for active area:', file = log_file)
				print('', file = log_file)
				for line in fit_result_active_area[k].fit_report().split('\n'):
					print('\t' + line, file = log_file)
				print('', file = log_file)
			
			# Plot quadrant fitting active area ------------------------
			print('Plotting results of active area of quadrant ' + str(k))
			fig_summary, ax = plt.subplots(nrows = 2, ncols = 1)
			fig_summary.suptitle('Quadrant ' + str(k) + ' calibration (active area)')
			fig_summary.set_label('quadrant ' + str(k) + ' calibration active area')
			ax[0].plot(x_vals_active_area[k], ecdf_active_area[k], label = 'ECDF')
			ax[0].plot(x_vals_active_area[k], model.eval(fit_result_active_area[k].params, x=x_vals_active_area[k]), label = 'Fit')
			ax[1].hist(active_area_data[k].ravel(), density = True, label = 'Hist ' + str(k), bins = 'auto')
			x_axis = np.linspace(-2,MAX_ELECTRONS_CALIB,300)
			ax[1].plot(x_axis[:-1], np.diff(model.eval(fit_result_active_area[k].params, x=x_axis))/np.diff(x_axis), label = 'Fit')
			for i in [0,1]:
				# ~ ax[i].set_xlim(-2, MAX_ELECTRONS_CALIB)
				ax[i].legend()
			ax[1].set_xlabel(r'# of $e^-$')
			x_axis = np.linspace(fit_result_active_area[k].params.get('shift').value, MAX_ELECTRONS_CALIB)
			ax[1].set_ylim(min(np.diff(model.eval(fit_result_active_area[k].params, x=x_axis))/np.diff(x_axis)), 5*max(np.diff(model.eval(fit_result_active_area[k].params, x=x_axis))/np.diff(x_axis)))
			ax[1].set_yscale('log')
			file_name = fig_summary.get_label().replace(' ', '_')
			fig_summary.savefig(calibration_results_dir + file_name + '.png')
			
			with open(calibration_results_dir + CALIBRATION_LOG_FILE_NAME, 'a') as log_file:
				print('![calibration plot for quadrant ' + str(k) + '](' + 'quadrant_' + str(k) + '_calibration_active_area.png)', file = log_file)
				print('', file = log_file)
			
			with open(calibration_results_dir + CALIBRATION_PARAMS_ACTIVE_AREA_FILE_NAME, 'a') as calib_params_file:
				print(str(k) + '\t' + str(fit_result_active_area[k].params['gain'].value) + '\t' + str(fit_result_active_area[k].params['shift'].value) + '\t' + str(fit_result_active_area[k].params['sigma'].value) + '\t' + str(fit_result_active_area[k].params['mu'].value), file = calib_params_file)
			
			# Processing overscan --------------------------------------
			overscan_data.append([row[CCDNPRES+int(CCDNCOL/2):NCOL] for row in hdu_list[k].data])
			overscan_data[k] = np.array(overscan_data[k]).ravel()/PSAMP
			overscan_data[k] = [i for i in overscan_data[k] if i > -10 and i < MAX_ELECTRONS_CALIB] # Assuming that there is not a very big shift and that the gain is approximately 1 (after dividing by PSAMP). Also discarding all the data for high occupancy pixels.
			overscan_data[k] = np.array(overscan_data[k]).ravel() # Cast from list to a numpy array.
			x_, ecdf_ = compute_ecdf(overscan_data[k])
			x_vals_overscan.append(x_)
			ecdf_overscan.append(ecdf_)
			
			# Fit model to oversacan -----------------------------------
			print('Fitting model to overscan of quadrant ' + str(k))
			params_overscan.append(model.make_params())
			params_overscan[k]['gain'].set(value = fit_result_active_area[k].params['gain'].value, max = 1.5, min = .5) # Initial guess
			params_overscan[k]['shift'].set(value = fit_result_active_area[k].params['shift'].value, max = 1, min = -1) # Initial guess
			params_overscan[k]['sigma'].set(value = fit_result_active_area[k].params['sigma'].value, min = 0) # Initial guess
			params_overscan[k]['mu'].set(value = fit_result_active_area[k].params['mu'].value, min = 0) # Initial guess
			fit_result_overscan.append(model.fit(ecdf_overscan[k], params_overscan[k], x = x_vals_overscan[k]))
			
			with open(calibration_results_dir + CALIBRATION_LOG_FILE_NAME, 'a') as log_file:
				print('### Overscan', file = log_file)
				print('', file = log_file)
				print('Gain equation params:', file = log_file)
				print('', file = log_file)
				print('- ```PSAMP``` = ' + str(PSAMP) + ' (from ' + filepath[filepath.rfind('/')+1:] + ' header)', file = log_file)
				print('- ```gain```: ' + str(fit_result_overscan[k].params['gain'].value) + ' (from fitting)', file = log_file)
				print('- ```shift```: ' + str(fit_result_overscan[k].params['shift'].value) + ' (from fitting)', file = log_file)
				print('', file = log_file)
				print('Fitting results for overscan:', file = log_file)
				print('', file = log_file)
				for line in fit_result_overscan[k].fit_report().split('\n'):
					print('\t' + line, file = log_file)
				print('', file = log_file)
			
			# Plot quadrant fitting overscan ---------------------------
			print('Plotting results of overscan of quadrant ' + str(k))
			fig_summary, ax = plt.subplots(nrows = 2, ncols = 1)
			fig_summary.suptitle('Quadrant ' + str(k) + ' calibration (overscan)')
			fig_summary.set_label('quadrant ' + str(k) + ' calibration overscan')
			ax[0].plot(x_vals_overscan[k], ecdf_overscan[k], label = 'ECDF')
			ax[0].plot(x_vals_overscan[k], model.eval(fit_result_overscan[k].params, x=x_vals_overscan[k]), label = 'Fit')
			ax[1].hist(overscan_data[k].ravel(), density = True, label = 'Hist ' + str(k), bins = 'auto')
			x_axis = np.linspace(-2,MAX_ELECTRONS_CALIB,300)
			ax[1].plot(x_axis[:-1], np.diff(model.eval(fit_result_overscan[k].params, x=x_axis))/np.diff(x_axis), label = 'Fit')
			for i in [0,1]:
				# ~ ax[i].set_xlim(-2, MAX_ELECTRONS_CALIB)
				ax[i].legend()
			ax[1].set_xlabel(r'# of $e^-$')
			x_axis = np.linspace(fit_result_overscan[k].params.get('shift').value, MAX_ELECTRONS_CALIB)
			ax[1].set_ylim(min(np.diff(model.eval(fit_result_overscan[k].params, x=x_axis))/np.diff(x_axis)), 5*max(np.diff(model.eval(fit_result_overscan[k].params, x=x_axis))/np.diff(x_axis)))
			ax[1].set_yscale('log')
			file_name = fig_summary.get_label().replace(' ', '_')
			fig_summary.savefig(calibration_results_dir + file_name + '.png')
			
			with open(calibration_results_dir + CALIBRATION_LOG_FILE_NAME, 'a') as log_file:
				print('![calibration plot for quadrant ' + str(k) + '](' + 'quadrant_' + str(k) + '_calibration_overscan.png)', file = log_file)
				print('', file = log_file)
			with open(calibration_results_dir + CALIBRATION_PARAMS_OVERSCAN_FILE_NAME, 'a') as calib_params_file:
				print(str(k) + '\t' + str(fit_result_overscan[k].params['gain'].value) + '\t' + str(fit_result_overscan[k].params['shift'].value) + '\t' + str(fit_result_overscan[k].params['sigma'].value) + '\t' + str(fit_result_overscan[k].params['mu'].value), file = calib_params_file)
			
			
		# Plot "all quadrants together" results ------------------------	
		fig_ecdf, ax = plt.subplots(nrows = 2, ncols = 2)
		fig_ecdf.suptitle('ECDF for active area of ' + filepath[filepath.rfind('/')+1:])
		fig_ecdf.set_label('ecdf active area all quadrants')
		for k in range(4):
			if k > len(ecdf_active_area)-1: break
			i, j = map_quadrant(k)
			ax[i][j].plot(x_vals_active_area[k], ecdf_active_area[k], label = 'ECDF ' + str(k))
			ax[i][j].plot(x_vals_active_area[k], model.eval(fit_result_active_area[k].params, x=x_vals_active_area[k]), label = 'Fit')
			ax[i][j].set_xlim(-2, MAX_ELECTRONS_CALIB)
			ax[i][j].legend()
			if i == 1:
				ax[i][j].set_xlabel(r'# of $e^-$')
		
		fig_histogram_active_area_linscale, ax = plt.subplots(nrows = 2, ncols = 2)
		fig_histogram_active_area_linscale.suptitle('Histogram for active area of ' + filepath[filepath.rfind('/')+1:])
		fig_histogram_active_area_linscale.set_label('histogram active area linscale all quadrants')
		for k in range(4):
			if k > len(ecdf_active_area)-1: break
			i, j = map_quadrant(k)
			ax[i][j].hist(active_area_data[k].ravel(), density = True, label = 'Hist ' + str(k), bins = 'auto') #, range = (-2,MAX_ELECTRONS_CALIB))
			x_axis = np.linspace(-2,MAX_ELECTRONS_CALIB,300)
			ax[i][j].plot(x_axis[:-1], np.diff(model.eval(fit_result_active_area[k].params, x=x_axis))/np.diff(x_axis), label = 'Fit')
			ax[i][j].set_xlim(-2, MAX_ELECTRONS_CALIB)
			ax[i][j].legend()
			if i == 1:
				ax[i][j].set_xlabel(r'# of $e^-$')
		
		fig_histogram_active_area_logscale, ax = plt.subplots(nrows = 2, ncols = 2)
		fig_histogram_active_area_logscale.suptitle('Histogram for active area of ' + filepath[filepath.rfind('/')+1:])
		fig_histogram_active_area_logscale.set_label('histogram active area logscale all quadrants')
		for k in range(4):
			if k > len(ecdf_active_area)-1: break
			i, j = map_quadrant(k)
			ax[i][j].hist(active_area_data[k].ravel(), density = True, label = 'Hist ' + str(k), bins = 'auto') #, range = (-2,MAX_ELECTRONS_CALIB))
			x_axis = np.linspace(-2,MAX_ELECTRONS_CALIB,300)
			ax[i][j].plot(x_axis[:-1], np.diff(model.eval(fit_result_active_area[k].params, x=x_axis))/np.diff(x_axis), label = 'Fit')
			ax[i][j].set_xlim(-2, MAX_ELECTRONS_CALIB)
			x_axis = np.linspace(fit_result_active_area[k].params.get('shift').value, MAX_ELECTRONS_CALIB)
			ax[i][j].set_ylim(min(np.diff(model.eval(fit_result_active_area[k].params, x=x_axis))/np.diff(x_axis)), 5*max(np.diff(model.eval(fit_result_active_area[k].params, x=x_axis))/np.diff(x_axis)))
			ax[i][j].set_yscale('log')
			ax[i][j].legend()
			if i == 1:
				ax[i][j].set_xlabel(r'# of $e^-$')
		
		fig_histogram_overscan, ax = plt.subplots(nrows = 2, ncols = 2)
		fig_histogram_overscan.suptitle('Histogram for overscan_data of ' + filepath[filepath.rfind('/')+1:])
		fig_histogram_overscan.set_label('histogram overscan_data all quadrants')
		for k in range(4):
			if k > len(ecdf_active_area)-1: break
			i, j = map_quadrant(k)
			ax[i][j].hist(overscan_data[k].ravel(), density = True, range = (-2,MAX_ELECTRONS_CALIB), bins = 'auto')
		
	# Save figures -----------------------------------------------------
	figs_list = [plt.figure(n) for n in plt.get_fignums()]
	for k in range(len(figs_list)):
		# ~ for ax in figs_list[k].axes:
			# ~ ax.grid(b=True, which='minor', color='#000000', alpha=0.1, linestyle='-', linewidth=0.25)
		file_name = figs_list[k].get_label().replace(' ', '_')
		figs_list[k].savefig(calibration_results_dir + file_name + '.png')
	
	with open(calibration_results_dir + CALIBRATION_LOG_FILE_NAME, 'a') as log_file:
		print('## Ordered quadrants', file = log_file)
		print('', file = log_file)
		print('![All quadrants ECDF]' + '(ecdf active area all quadrants.png)'.replace(' ', '_'), file = log_file)
		print('![All quadrants histogram]' + '(histogram active area linscale all quadrants.png)'.replace(' ', '_'), file = log_file)

# Script starts here ---------------------------------------------------

directory_or_file = sys.argv[1]

if '.fits' in directory_or_file:
	process_file(directory_or_file)
else:
	if directory_or_file[-1] != '/':
		directory_or_file += '/'
	for file in os.listdir(directory_or_file):
		if '.fits' in file:
			process_file(directory_or_file + file)
