RESULTS_DIR_SUFFIX = '-pizza_correlation'

########################################################################

import numpy as np
import matplotlib.pyplot as plt

class pizza_cooker:
	def __init__(self, img_shape, pizza_center, pizza_radius, pizza_sigma, pizza_N_portions):
		self.img_shape = img_shape
		self.pizza_center = pizza_center
		self.pizza_radius = pizza_radius
		self.pizza_sigma = pizza_sigma
		self.pizza_N_portions = pizza_N_portions
		self.__r_matrix = build_radius_matrix(center = pizza_center, matrix_size = img_shape)
		self.__theta_matrix = build_theta_matrix(center = pizza_center, matrix_size = img_shape)
		self.ring_mask = build_ring_mask(r = pizza_radius, sigma = pizza_sigma, r_matrix = self.__r_matrix)
		self.portions_masks = build_portions_mask(img_shape, pizza_N_portions, self.__theta_matrix)
	
	def ring_portion_mask(self, k):
		mask = np.zeros(self.img_shape)
		mask[self.portions_masks!=k] = -1
		mask[self.portions_masks==k] = 1
		mask[mask==-1] = 0
		mask = self.ring_mask*mask
		return mask
	
def build_theta_matrix(center=[0,0], matrix_size=[100,100]):
	matrix = np.zeros(matrix_size)
	for x in range(matrix.shape[0]):
		for y in range(matrix.shape[1]):
			matrix[x][y] = np.arctan2(y-center[1], x-center[0])
	matrix[matrix < 0] += 2*np.pi
	return matrix

def build_radius_matrix(center=[0,0], matrix_size=[100,100]):
	matrix = np.zeros(matrix_size)
	for x in range(matrix.shape[0]):
		for y in range(matrix.shape[1]):
			matrix[x][y] = ((x-center[0])**2 + (y-center[1])**2)**.5
	return matrix

def build_r_and_theta(center=[0,0], matrix_size=[100,100]):
	return build_radius_matrix(center, matrix_size), build_theta_matrix(center, matrix_size)

def build_ring_mask(r, sigma, r_matrix):
	mask = np.ones(r_matrix.shape)
	mask[r_matrix<r-sigma] = 0
	mask[r_matrix>r+sigma] = 0
	return mask

def build_portion_mask(portion_number, N_portions, theta_matrix):
	mask = np.ones(theta_matrix.shape)
	mask[theta_matrix<2*np.pi*portion_number/N_portions] = 0
	mask[theta_matrix>2*np.pi*(portion_number+1)/N_portions] = 0
	return mask

def build_portions_mask(img_shape, pizza_N_portions, theta_matrix):
	mask = np.zeros(img_shape) - 1
	for k in range(pizza_N_portions):
		mask[build_portion_mask(k, pizza_N_portions, theta_matrix) == 1] = k
	return np.array(mask, dtype = int)

def plot_img_and_pizza(img, pizza):
	fig, ax = plt.subplots()
	fig.suptitle('Data and pizza')
	ax.imshow(pizza.ring_mask)
	ax.imshow(pizza.ring_portion_mask(0) + pizza.ring_portion_mask(int(pizza.pizza_N_portions/2)), alpha = .5)
	ax.imshow(img, alpha = .3)
	return fig, ax

def circular_correlation(vec, position=None):
	if position is None:
		correlation = []
		for k in range(len(vec)):
			correlation.append(circular_correlation(vec, k))
		return correlation
	else:
		vec = np.array(vec)
		vec_shifted = list(vec[position%len(vec):]) + list(vec[:position%len(vec)])
		vec_shifted = np.array(vec_shifted)
		return (vec*vec_shifted).sum()/(vec**2).sum()

########################################################################

if __name__ == '__main__':
	import os
	import sys
	from astropy.io import fits
	import argparse
	
	parser = argparse.ArgumentParser(description='Searchs for correlation using the pizza method.')
	parser.add_argument('--fits',
						metavar = 'file', 
						help = 'Path to a FITS file.',
						required = True,
						dest = 'fits_file_name'
						)
	parser.add_argument('--circ',
						metavar = 'file', 
						help = 'Path to a file generated by the "circle_finder.py" script.',
						required = True,
						dest = 'circ_file_name'
						)
	parser.add_argument('--portions',
						metavar = 'N', 
						help = 'Numer of portions wanted.',
						dest = 'N_portions',
						type = int,
						default = 8
						)

	args = parser.parse_args()

	FILE = args.fits_file_name

	data = fits.open(FILE)[0].data

	# ~ data[data>20e3] = 0 # Remove muons
	
	with open(args.circ_file_name, 'r') as f:
		for line in f:
			line = line.split(': ')
			if 'y_center' in line:
				x_center = float(line[1])
			if 'x_center' in line:
				y_center = float(line[1])
			if 'radius' in line:
				radius = float(line[1])
			if 'sigma' in line:
				sigma = float(line[1])
	
	big_of_muzza = pizza_cooker( # Anillos medidos
								data.shape, 
								pizza_center = [x_center, y_center],
								pizza_radius = radius,
								pizza_sigma = sigma,
								pizza_N_portions = args.N_portions
								)
	
	os.mkdir(FILE[:-5] + RESULTS_DIR_SUFFIX)
	fig, ax = plot_img_and_pizza(data, big_of_muzza)
	fig.savefig(FILE[:-5] + RESULTS_DIR_SUFFIX + '/data_and_pizza.png')

	portions_amplitude = []
	for k in range(big_of_muzza.pizza_N_portions):
		portions_amplitude.append(np.nanmean(data*big_of_muzza.ring_portion_mask(k)))
	portions_amplitude = np.array(portions_amplitude)
	
	correlation = circular_correlation(portions_amplitude - portions_amplitude.mean())
	
	fig, ax = plt.subplots()
	fig.suptitle('Portions amplitude')
	ax.plot([k*360/big_of_muzza.pizza_N_portions for k in range(big_of_muzza.pizza_N_portions)], portions_amplitude, marker='.')
	ax.set_xlabel('Angle of portion')
	ax.set_ylabel('Amplitude')
	fig.savefig(FILE[:-5] + RESULTS_DIR_SUFFIX + '/portions_amplitude.png')

	fig, ax = plt.subplots()
	fig.suptitle('Correlation')
	ax.plot([i*360/big_of_muzza.pizza_N_portions for i in range(len(correlation))], correlation, marker='.')
	ax.set_xlabel('$n$ (degres)')
	ax.set_ylabel('Correlation')
	fig.savefig(FILE[:-5] + RESULTS_DIR_SUFFIX + '/correlation.png')
	
	plt.show()
