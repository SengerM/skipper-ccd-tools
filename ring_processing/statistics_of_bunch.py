AVAILABLE_OPERATIONS = ['mean', 'median', 'nanmean']

########################################################################

import os
import sys
from astropy.io import fits
import numpy as np
import matplotlib.pyplot as plt
import argparse

parser = argparse.ArgumentParser(description = 'Given a directory with multiple FITS files this program calculates statistical quantities for each pixel in a "filewise" way.')
parser.add_argument('--dir',
					metavar = 'directory', 
					help = 'Directory containing the FITS files.',
					required = True,
					dest = 'dir'
					)
parser.add_argument('--op',
					help = 'Which operation to perform. See the "numpy" documentation for each case.',
					choices = AVAILABLE_OPERATIONS,
					dest = 'operation',
					required = True
					)

args = parser.parse_args()

directory = args.dir

if directory[-1] != '/':
	directory += '/'

hduls = []
filenames = []
for file in os.listdir(directory):
	if '.fits' in file:
		hduls.append(fits.open(directory + file))
		filenames.append(file)

data = []
data.append([hdul[0].data for hdul in hduls])

if args.operation == AVAILABLE_OPERATIONS[0]: # mean
	oimage = fits.PrimaryHDU(np.mean([hdul[0].data for hdul in hduls], 0))
	oimage.writeto(directory + 'mean.fits')
if args.operation == AVAILABLE_OPERATIONS[1]: # median
	oimage = fits.PrimaryHDU(np.median([hdul[0].data for hdul in hduls], 0))
	oimage.writeto(directory + 'median.fits')
if args.operation == AVAILABLE_OPERATIONS[2]: # mean
	oimage = fits.PrimaryHDU(np.nanmean([hdul[0].data for hdul in hduls], 0))
	oimage.writeto(directory + 'nanmean.fits')
