PROCESSED_DIR_NAME = 'processed'
ONLY_QUADRANT_0_PY_PATH = '/home/darmat/soft/skipper-ccd-tools/only_quadrant_0.py'
SUBTRACT_OVERSCAN_PY_PATH = '/home/darmat/soft/skipper-ccd-tools/subtract_overscan.py'
STATISTICS_OF_BUNCH_PY_PATH = '/home/darmat/soft/skipper-ccd-tools/ring_processing/statistics_of_bunch.py'
OPERATION_BETWEEN_IMAGES_PY_PATH = '/home/darmat/soft/skipper-ccd-tools/ring_processing/operation_between_images.py'
MASK_MUONS_PY_PATH = '/home/darmat/soft/skipper-ccd-tools/mask_muons.py'
RING_FITTER_PY_PATH = '/home/darmat/soft/skipper-ccd-tools/ring_processing/ring_fitter.py'

########################################################################

import os
import sys
from astropy.io import fits
import numpy as np
import matplotlib.pyplot as plt
import argparse

parser = argparse.ArgumentParser(description='Process a bunch of FITS images looking for the ring.')
parser.add_argument('--dir',
					metavar = 'directory', 
					help = 'Path to the directory containing the bunch of FITS files',
					required = True,
					dest = 'dir'
					)
parser.add_argument('--umbral',
					metavar = 'umbral',
					help = 'Value of argument "--umbral" for passing to the script "mask_muons.py". Default is 100e3.',
					dest = 'umbral',
					type = float,
					default = 100e3
					)

args = parser.parse_args()

directory_with_files = args.dir
directory_with_files += '' if directory_with_files[-1] == '/' else '/'

lta_files = os.listdir(directory_with_files)
lta_files = [file for file in lta_files if '.fits' in file]
lta_files.sort()

os.mkdir(directory_with_files + PROCESSED_DIR_NAME)

os.system('python3 ' + ONLY_QUADRANT_0_PY_PATH + ' -i ' + directory_with_files)
for file in os.listdir(directory_with_files):
	if '.fits' not in file:
		continue
	if file not in lta_files:
		os.system('mv ' + directory_with_files + file + ' ' + directory_with_files + PROCESSED_DIR_NAME)

files2process = os.listdir(directory_with_files + PROCESSED_DIR_NAME)
files2process.sort()

os.system('python3 ' + SUBTRACT_OVERSCAN_PY_PATH + ' --remove-overscan -i ' + directory_with_files + PROCESSED_DIR_NAME)

for file in files2process:
	os.system('rm ' + directory_with_files + PROCESSED_DIR_NAME + '/' + file)

files_with_overscan_subtracted = os.listdir(directory_with_files + PROCESSED_DIR_NAME)
files_with_overscan_subtracted.sort()

os.system('python3 ' + STATISTICS_OF_BUNCH_PY_PATH + ' --op median --dir ' + directory_with_files + PROCESSED_DIR_NAME)
os.system('mv ' + directory_with_files + PROCESSED_DIR_NAME + '/median.fits ' + directory_with_files + PROCESSED_DIR_NAME + '/subtracted_overscan-median.fits')

files_with_overscan_subtracted_and_median_subtracted = [None]*len(files_with_overscan_subtracted)
for k,file in enumerate(files_with_overscan_subtracted):
	os.system(
			  'python3 ' + OPERATION_BETWEEN_IMAGES_PY_PATH + 
			  ' --fits1 ' + directory_with_files + PROCESSED_DIR_NAME + '/' + file + 
			  ' --fits2 ' + directory_with_files + PROCESSED_DIR_NAME + '/subtracted_overscan-median.fits' + 
			  ' --op sub' + 
			  ' --ofile ' + file[:-5] + '-median_subtracted.fits'
			 )
	files_with_overscan_subtracted_and_median_subtracted[k] = file[:-5] + '-median_subtracted.fits'

files_with_muons_removed = []
for k,file in enumerate(files_with_overscan_subtracted):
	os.system(
			   'python3 ' + MASK_MUONS_PY_PATH + 
			   ' --mask-in ' + directory_with_files + PROCESSED_DIR_NAME + '/' + file +
			   ' --look-in ' + directory_with_files + PROCESSED_DIR_NAME + '/' + files_with_overscan_subtracted_and_median_subtracted[k] + 
			   ' --umbral ' + str(args.umbral)
			 )
	files_with_muons_removed.append(file[:-5] + '-Muons_removed.fits')

os.mkdir(directory_with_files + PROCESSED_DIR_NAME + '/overscan_subtracted-median_subtracted')
for file in files_with_overscan_subtracted_and_median_subtracted:
	os.system(
			  'mv ' + 
			  directory_with_files + PROCESSED_DIR_NAME + '/' + file + ' ' + 
			  directory_with_files + PROCESSED_DIR_NAME + '/overscan_subtracted-median_subtracted'
			 )

os.system('python3 ' + RING_FITTER_PY_PATH + ' --fits ' + directory_with_files + PROCESSED_DIR_NAME + '/subtracted_overscan-median.fits')

os.system('mkdir ' + directory_with_files + PROCESSED_DIR_NAME + '/muons_removed_imgs')
for file in files_with_muons_removed:
	os.system('mv ' + directory_with_files + PROCESSED_DIR_NAME + '/' + file + ' ' + directory_with_files + PROCESSED_DIR_NAME + '/muons_removed_imgs')

os.system('python3 ' + STATISTICS_OF_BUNCH_PY_PATH + ' --op nanmean --dir ' + directory_with_files + PROCESSED_DIR_NAME + '/muons_removed_imgs')

files_with_overscan_subtracted_and_nanmean_subtracted = []
for file in files_with_muons_removed: 
	os.system(
			  'python3 ' + OPERATION_BETWEEN_IMAGES_PY_PATH + 
			  ' --fits1 ' + directory_with_files + PROCESSED_DIR_NAME + '/muons_removed_imgs/' + file + 
			  ' --fits2 ' + directory_with_files + PROCESSED_DIR_NAME + '/muons_removed_imgs/nanmean.fits' + 
			  ' --op sub' + 
			  ' --ofile ' + file[:-5] + '-nanmean_subtracted.fits'
			 )

