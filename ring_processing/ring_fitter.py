RESULTS_DIRECTORY_SUFFIX = '-circle_found_parameters'

########################################################################

import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import find_peaks
from scipy.signal import decimate
from scipy.stats import norm
import lmfit
import matplotlib.pyplot as plt
from astropy.io import fits
import sys
import os
import argparse

def ring_center_raw_estimation(data):
	data = data - data.mean().mean()
	data[data < 0] = 0
	projected_in_x = data.sum(axis = 0)
	projected_in_y = data.sum(axis = 1)
	x_center = sum([i*projected_in_x[i] for i in range(len(projected_in_x))])/sum(projected_in_x)
	y_center = sum([i*projected_in_y[i] for i in range(len(projected_in_y))])/sum(projected_in_y)
	return (x_center, y_center)

def ring_radius_raw_estimation(data):
	data = data - data.mean().mean()
	data[data < 0] = 0
	
	peaks_pixels, _ = find_peaks(data.sum(axis = 0), height = data.sum(axis = 0).mean())
	peaks_vals = data.sum(axis = 0)[peaks_pixels]
	
	return np.abs(peaks_pixels[0] - peaks_pixels[-1])/2

def gaussian_ring(x, y, x_center=0, y_center=0, radius=1, sigma=.1):
	return norm.pdf(((x-x_center)**2+(y-y_center)**2)**.5, loc = radius, scale = sigma)

def square_ring(x, y, x_center=0, y_center=0, radius=1, sigma=.1):
	R = ((x-x_center)**2+(y-y_center)**2)**.5
	return (R<radius+sigma)*(R>radius-sigma)

def ring_model_for_lmfit(x, x_center, y_center, radius, sigma, amplitude, offset, x_pixels, y_pixels):
	pixel_number = x # This is the pixel number in the data array
	# x_pixels and y_pixels are the number of pixels that the image has. This is needed to make the mapping.
	if (pixel_number > x_pixels*y_pixels - 1).any():
		raise ValueError('pixel_number (x) > x_pixels*y_pixels - 1')
	x = [int(p%x_pixels) for p in pixel_number]
	y = [int(p/x_pixels) for p in pixel_number]
	return amplitude*gaussian_ring(np.array(x), np.array(y), x_center, y_center, radius, sigma) + offset

def fit_circle(data, x_center_guess, y_center_guess, radius_guess, sigma_guess,  decimation_factor = 1):
	if decimation_factor == 'auto':
		maximum_megapixels_we_want_to_handle = 0.1
		decimation_factor = int(data.shape[0]*data.shape[1]/(maximum_megapixels_we_want_to_handle*1e6))
		if decimation_factor == 0:
			decimation_factor = 1
	data = np.array(data)
	data = decimate(decimate(data, decimation_factor, axis = 0), decimation_factor, axis = 1)
	data -= data.min().min()
	data = data/data.max().max()
	model = lmfit.Model(ring_model_for_lmfit)
	params = model.make_params()
	params['x_center'].set(value = x_center_guess/decimation_factor, min = 0, max = data.shape[1])
	params['y_center'].set(value = y_center_guess/decimation_factor, min = 0, max = data.shape[0])
	params['radius'].set(value = radius_guess/decimation_factor, min = 0)
	params['sigma'].set(value = sigma_guess/decimation_factor, min = 0)
	params['x_pixels'].set(value = data.shape[1], vary = False)
	params['y_pixels'].set(value = data.shape[0], vary = False)
	params['amplitude'].set(value = 1, vary = True) # We are not interested in this parameter, but it may be usefull to have it.
	params['offset'].set(value = 0, vary = True) # We are not interested in this parameter, but it may be usefull to have it.
	fit_results = model.fit(data.ravel(), params, x = [i for i in range(len(data.ravel()))])
	return {'x_center':fit_results.params['x_center']*decimation_factor, 'y_center':fit_results.params['y_center']*decimation_factor, 'radius':fit_results.params['radius']*decimation_factor, 'sigma':fit_results.params['sigma']*decimation_factor, 'amplitude': fit_results.params['amplitude']*1, 'offset': fit_results.params['offset']*1}

def plot_data_and_circle(data, circle_params):
	x_center = circle_params.get('x_center')
	y_center = circle_params.get('y_center')
	radius = circle_params.get('radius')
	sigma = circle_params.get('sigma')
	fig, ax = plt.subplots()
	ax.imshow(data)
	# ~ x = [i for i in range(data.shape[1])]
	# ~ y = [i for i in range(data.shape[0])]
	# ~ xx, yy = np.meshgrid(x, y, sparse=True)
	# ~ ax.contourf(x,y,
		# ~ gaussian_ring(np.array(xx),np.array(yy),
			# ~ x_center = x_center,
			# ~ y_center = y_center,
			# ~ radius = radius,
			# ~ sigma = sigma
		# ~ ),
		# ~ alpha = .3
	# ~ )
	ax.add_artist(plt.Circle((x_center, y_center), radius+sigma, color=(1,1,1), fill=False, linestyle='--'))
	ax.add_artist(plt.Circle((x_center, y_center), radius-sigma, color=(1,1,1), fill=False, linestyle='--'))
	ax.add_artist(plt.Circle((x_center, y_center), radius, color=(1,1,1), fill=False))
	ax.text(0.05, 0.95, 
		'x = {:.2e}'.format(x_center) + '\ny = {:.2e}'.format(y_center) + '\nr = {:.2e}'.format(radius) + '\n' + r'$\sigma$ = ' + '{:.2e}'.format(sigma),
		transform=ax.transAxes, 
		verticalalignment='top',
		color = (1,1,1)
	)
	ax.set_xlabel('Pixel number')
	ax.set_ylabel('Pixel number')
	return fig, ax

########################################################################

parser = argparse.ArgumentParser(description = 'Fits a gaussian ring to data in a FITS image using least squares.')
parser.add_argument('--fits',
					metavar = 'file', 
					help = 'Path to the file you want the ring to be fitted.',
					required = True,
					dest = 'file2process'
					)
parser.add_argument('-x',
					metavar = 'x_center',
					help = 'Value to use as "starting guess" for the x coordinate of the center of the ring. Default is to use "autoguess".',
					dest = 'x_center',
					type = float,
					default = float('nan')
					)
parser.add_argument('-y',
					metavar = 'y_center',
					help = 'Value to use as "starting guess" for the y coordinate of the center of the ring. Default is to use "autoguess".',
					dest = 'y_center',
					type = float,
					default = float('nan')
					)
parser.add_argument('-r',
					metavar = 'radius',
					help = 'Value to use as "starting guess" for the radius of the ring. Default is to use "autoguess".',
					dest = 'radius',
					type = float,
					default = float('nan')
					)
parser.add_argument('-s',
					metavar = 'sigma',
					help = 'Value to use as "starting guess" for the sigma of the ring. Default is to use "radius*0.2".',
					dest = 'sigma',
					type = float,
					default = float('nan')
					)
parser.add_argument('-v',
					help = 'Verbose, prints messages.',
					dest = 'verbose',
					action = 'store_true',
					default = False
					)

args = parser.parse_args()

file2process = args.file2process

if args.verbose: print('Reading file...')
with fits.open(file2process) as hdul:
	data = hdul[0].data

center_x_raw_guess, center_y_raw_guess = ring_center_raw_estimation(data)
radius_raw_guess = ring_radius_raw_estimation(data)
x_guess = center_x_raw_guess if np.isnan(args.x_center) else args.x_center
y_guess = center_y_raw_guess if np.isnan(args.y_center) else args.y_center
r_guess = radius_raw_guess if np.isnan(args.radius) else args.radius
sigma_guess = radius_raw_guess*.2 if np.isnan(args.sigma) else args.sigma

os.mkdir(file2process[:-5] + RESULTS_DIRECTORY_SUFFIX)
fig, ax = plot_data_and_circle(data, {'x_center':x_guess, 'y_center':y_guess, 'radius':r_guess, 'sigma':sigma_guess})
fig.suptitle('Guessed ring')
fig.savefig(file2process[:-5] + RESULTS_DIRECTORY_SUFFIX + '/guessed_ring.png')

if args.verbose: print('Fitting ring...')
fitted_circle_params = fit_circle(data, x_center_guess=x_guess, y_center_guess=y_guess, radius_guess=r_guess, sigma_guess=sigma_guess, decimation_factor = 'auto')
if args.verbose: print('Ring fittes!')

x_center = fitted_circle_params.get('x_center')
y_center = fitted_circle_params.get('y_center')
radius = fitted_circle_params.get('radius')
sigma = fitted_circle_params.get('sigma')
amplitude = fitted_circle_params.get('amplitude')
offset = fitted_circle_params.get('offset')

fig, ax = plot_data_and_circle(data, fitted_circle_params)
fig.suptitle('Fitted ring')
fig.savefig(file2process[:-5] + RESULTS_DIRECTORY_SUFFIX + '/fitted_ring.png')

with open(file2process[:-5] + RESULTS_DIRECTORY_SUFFIX + '/fitted_ring_params.txt', 'w') as ofile:
	print('x_center: ' + str(x_center), file = ofile)
	print('y_center: ' + str(y_center), file = ofile)
	print('radius: ' + str(radius), file = ofile)
	print('sigma: ' + str(sigma), file = ofile)
	print('amplitude: ' + str(amplitude), file = ofile)
	print('offset: ' + str(offset), file = ofile)

if args.verbose: print('Results have been saved in directory "' + file2process[:-5] + RESULTS_DIRECTORY_SUFFIX + '"')
