import numpy as np
import matplotlib.pyplot as plt

def Upsilon(img, x, y):
	A = np.array(img)
	B = np.rot90(np.rot90(A))
	
	if x > 0:
		A = A[x:,:]
		B = B[:-x,:]
	elif x < 0:
		A = A[:x,:]
		B = B[-x:,:]
	if y > 0:
		A = A[:,y:]
		B = B[:,:-y]
	elif y < 0:
		A = A[:,:y]
		B = B[:,-y:]
	
	if __name__ == '__main__': # For debugging purposes
		print('A = ')
		print(A)
		print('B = ')
		print(B)
	
	return (A*B).mean()

if __name__ == '__main__':
	A = [
	[1,2,3,4,5],
	[6,7,8,9,10],
	[11,12,13,14,15],
	[16,17,18,19,20],
	]

	A = np.array(A)
	print(A)

	Upsilon(A, -2, -2)
