RESULTS_DIR_NAME = 'statistical_images'
DEFAULT_RESULT_FILE_NAME = 'result.fits'
AVAILABLE_OPERATIONS = ['add', 'sub', 'prod', 'div']

########################################################################

from astropy.io import fits
import numpy as np
import argparse

parser = argparse.ArgumentParser(description = 'Perform a mathematical binary operation pixelwise between two images. For example, sum two image pixelwise.')
parser.add_argument('--fits1',
					metavar = 'file', 
					help = 'First FITS image for the operation.',
					required = True,
					dest = 'fits1'
					)
parser.add_argument('--fits2',
					metavar = 'file', 
					help = 'Second FITS image for the operation.',
					required = True,
					dest = 'fits2'
					)
parser.add_argument('--op',
					help = 'Which operation to perform between the pixels.',
					choices = AVAILABLE_OPERATIONS,
					dest = 'operation',
					required = True
					)
parser.add_argument('--ofile',
					metavar = 'file name', 
					help = 'Name of the resulting image file. Default is "' + DEFAULT_RESULT_FILE_NAME + '".',
					dest = 'ofile'
					)

args = parser.parse_args()

with fits.open(args.fits1) as hdul_1:
	with fits.open(args.fits2) as hdul_2:
		for k in range(len(hdul_1)):
			if args.operation == AVAILABLE_OPERATIONS[0]:
				hdul_2[k].data = hdul_1[k].data + hdul_2[k].data
			if args.operation == AVAILABLE_OPERATIONS[1]:
				hdul_2[k].data = hdul_1[k].data - hdul_2[k].data
			if args.operation == AVAILABLE_OPERATIONS[2]:
				hdul_2[k].data = hdul_1[k].data * hdul_2[k].data
			if args.operation == AVAILABLE_OPERATIONS[3]:
				hdul_2[k].data = hdul_1[k].data / hdul_2[k].data
		ofilename = DEFAULT_RESULT_FILE_NAME if args.ofile == None else args.ofile
		ofilename += '.fits' if ofilename[-5:] != '.fits' else ''
		hdul_2.writeto(args.fits2[:args.fits2.rfind('/')+1] + ofilename)
