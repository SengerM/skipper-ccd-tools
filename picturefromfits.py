import os
import sys

directory_or_file = sys.argv[1]

def process_file(file):
	os.system('python3 fits2jpeg.py ' + file)
	os.system('python3 combinequadrants.py ' + file)
	for k in [0,1,2,3]:
		os.system('rm ' + file[:-5] + '_' + str(k) + '.jpeg')

if '.fits' in directory_or_file: # In this case this is just a file
	process_file(directory_or_file)
else: # We have to process all the directory
	if directory_or_file[-1] != '/':
		directory_or_file += '/'
	for file in os.listdir(directory_or_file):
		if '.fits' in file:
			process_file(directory_or_file + file)
