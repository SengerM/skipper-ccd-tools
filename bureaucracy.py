from timestamp import get_timestamp
import os

BASE_DIR = '/home/darmat/lta_reads'

########################################################################

BASE_DIR = BASE_DIR if BASE_DIR[-1] == '/' else BASE_DIR + '/'

def get_reading_dir(dirname=None, current_script_path=None):
	if dirname is None:
		raise ValueError('You must provide a name for the directory')
	if current_script_path is None:
		raise ValueError('Missing argument "current_script_path". Please provide the FULL PATH of your current script. You can do this easilly with "current_script_path = os.path.realpath(__file__)". (This is in order to create a copy of your script inside the reading directory, some day you\'ll be glad of this.)')
	now = get_timestamp()
	if now[:6] not in os.listdir(BASE_DIR):
		os.mkdir(BASE_DIR + now[:6])
	reading_directory = BASE_DIR + now[:6] + '/' + now[6:] + '_' + dirname + '/'
	os.mkdir(reading_directory)
	print('Created directory ' + reading_directory)
	os.system('cp ' + current_script_path + ' ' + reading_directory)
	print('Copied script ' + current_script_path + ' to reading directory')
	return reading_directory
