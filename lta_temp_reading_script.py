import os
import ltapy
from time import sleep
import numpy as np
from bureaucracy import *
import lakeshore

print('Starting script!')

# ~ reading_directory = get_reading_dir(dirname = input('Insert title for this measurement: ').replace(' ', '_'), current_script_path = os.path.realpath(__file__))

# ~ lta = ltapy.lta(reading_directory=reading_directory)

lakeshore.ctrl.LED(status = 'on') # Prendo el LED a maxima potencia
lakeshore.ctrl.LED(status = 'off') # Apago el LED
lakeshore.ctrl.LED(voltage = 4) # Prendo el LED con menos potencia, lo maximo son 5 V y aca le mando 4 V
lakeshore.ctrl.LED(status = 'off') # Apago el LED
lakeshore.ctrl.LED(status = 'on') # Prendo el LED a maxima potencia
# Ahora no hace falta apagarlo, cuando termina el script se apaga automaticamente (lo hace la clase lakeshore)
