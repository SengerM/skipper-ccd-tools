#include <string.h>
#include <stdio.h>
#include "fitsio.h"

#include <iostream>
#include <sstream>

#include <inttypes.h>
#include <fstream>
#include <unistd.h>
#include <getopt.h>    /* for getopt_long; standard getopt is in unistd.h */
#include <vector>
#include <algorithm>
#include <cmath>
#include <iomanip>


bool verbosity = true;

//colors
const char cyan[] = { 0x1b, '[', '1', ';', '3', '6', 'm', 0 };
const char magenta[] = { 0x1b, '[', '1', ';', '3', '5', 'm', 0 };
const char red[] = { 0x1b, '[', '1', ';', '3', '1', 'm', 0 };
const char green[] = { 0x1b, '[', '1', ';', '3', '2', 'm', 0 };
const char yellow[] = { 0x1b, '[', '1', ';', '3', '3', 'm', 0 };
const char blue[] = "\x1b[1;34m";

const char bold[] = "\x1b[1;39m";

const char whiteOnRed[]    = "\x1b[1;41m";
const char whiteOnGreen[]  = "\x1b[1;42m";
const char whiteOnPurple[] = "\x1b[1;45m";
const char whiteOnViolet[] = "\x1b[1;44m";
const char whiteOnBrown[]  = "\x1b[1;43m";
const char whiteOnGray[]   = "\x1b[1;47m";

const char normal[] = { 0x1b, '[', '0', ';', '3', '9', 'm', 0 };


using namespace std;

int deleteFile(const char *fileName){
    cout << yellow;
    cout << "Will overwrite: " << fileName << endl << endl;
    cout << normal;
    return unlink(fileName);
}

bool fileExist(const char *fileName){
    ifstream in(fileName,ios::in);
    
    if(in.fail()){
        //cout <<"\nError reading file: " << fileName <<"\nThe file doesn't exist!\n\n";
        in.close();
        return false;
    }
    
    in.close();
    return true;
}

void printCopyHelp(const char *exeName, bool printFullHelp=false){
    
    if(printFullHelp){
        cout << bold;
        cout << endl;
        cout << "This program computes basic statistics on a region of pixels\n";
        cout << normal;
    }
    cout << "==========================================================================\n";
    cout << yellow;
    cout << "\nUsage:\n";
    cout << "  "   << exeName << " <input file> [initCol:finCol,initRow:finRow] \n";
    cout << "\nOptions:\n";
    cout << "  -q for quiet less screen output\n";
    cout << "  -s <HDU number>\n";
    cout << "  -m Mean value\n";
    cout << "  -n Median value\n";
    cout << "  -o Std. Dev. value\n";
    cout << "  -p Mad value\n";
    cout << "  -a Compute all values (default)\n";
    cout << normal;
    cout << "==========================================================================\n\n";
}

void computeMean(vector<double> &vArray,double &mean){
    double sum=0;
    const long N = vArray.size();
    for(unsigned int i=0;i<N;i++){
        sum = sum + vArray[i];
    }
    mean = sum/N;
}
void computeStd(vector<double> &vArray,double &stdev){
    double mean;
    computeMean(vArray,mean);
    
    const long N = vArray.size();
    double quadSum=0;
    for(unsigned int i=0;i<N;i++){
        quadSum = quadSum + pow(vArray[i]-mean,2.0);
    }
    
    stdev = sqrt(quadSum/(N-1));
}

void computeMedian(vector<double> &vArray,double &median){
    
    const long N = vArray.size();
    bool NOdd = !!(N & 1);
    const long m = N/2;
    if (NOdd){
        std::nth_element(vArray.begin(), vArray.begin()+vArray.size()/2, vArray.end());
        median = vArray[m];
    }else {
        std::nth_element(vArray.begin(), vArray.begin()+vArray.size()/2, vArray.end());
        const double highMedianVal = vArray[m];
        std::nth_element(vArray.begin(), vArray.begin()+vArray.size()/2-1, vArray.end());
        const double lowMedianVal  = vArray[m-1];
        median = (highMedianVal+lowMedianVal)/2.;
        
    }
}

void computeMad(vector<double> &vArray,double &mad){
    double median;
    computeMedian(vArray, median);
    
    const long N = vArray.size();
    for(unsigned int i=0;i<N;i++){
        vArray[i] = fabs(vArray[i]-median);
    }
    
    bool NOdd = !!(N & 1);
    const long m = N/2;
    if (NOdd){
        std::nth_element(vArray.begin(), vArray.begin()+vArray.size()/2, vArray.end());
        mad = vArray[m];
    }else {
        std::nth_element(vArray.begin(), vArray.begin()+vArray.size()/2, vArray.end());
        const double highMadVal = vArray[m];
        std::nth_element(vArray.begin(), vArray.begin()+vArray.size()/2-1, vArray.end());
        const double lowMadVal  = vArray[m-1];
        mad = (highMadVal+lowMadVal)/2.;
        
    }
    
    
}

int noiseSample(const char *inF, vector<int> &singleHdu, long *fpixel, long *lpixel, const string kMode, vector< vector <double> > &output, vector<string> &outputDesc, const unsigned int nOutVars){
  
    fitsfile *infptr;   /* FITS file pointers defined in fitsio.h */
    
    int status = 0;
    int nhdu = 0;
    
    /*open fits file*/
    fits_open_file(&infptr, inF, READONLY, &status); /* Open the input file */
    if (status != 0) return(status);
    
    /* check extensions to process*/
    fits_get_num_hdus(infptr, &nhdu, &status);
    
    if(singleHdu.size() == 0){
        for(int i=0;i<nhdu;++i){
            singleHdu.push_back(i+1);
        }
    }
    
    for(unsigned int i=0;i<singleHdu.size();++i){
        if(singleHdu[i] > nhdu){
            fits_close_file(infptr,  &status);
            cerr << red << "\nError: the file does not have the required HDU!\n\n" << normal;
            return -1000;
        }
    }
    
    const unsigned nUseHdu = singleHdu.size();
        
    /*prepare output*/
    vector<double> temp;
    temp.assign(nOutVars,NAN);
    output.assign(nUseHdu,temp);
    outputDesc.assign(nUseHdu,"empty string");
    
    /* process region of interest*/
    long nPix = (lpixel[0]-fpixel[0]+1)*(lpixel[1]-fpixel[1]+1);
    long inc[2] = {1,1};
    double nulval = 0.;
    int anynul;
    double *array = new double[nPix];
    for (unsigned int iHdu=0; iHdu<nUseHdu;iHdu++){ /*main loop through each extension*/
        
        /*moves to HDU*/
        const unsigned int n = singleHdu[iHdu];
        int hdutype, bitpix, naxis = 0;
        long naxes[9] = {1, 1, 1, 1, 1, 1, 1, 1, 1};
        fits_movabs_hdu(infptr, n, &hdutype, &status);
        for (int j = 0; j < 9; ++j) naxes[j] = 1;
        fits_get_img_param(infptr, 9, &bitpix, &naxis, naxes, &status);
        
        /*check if it is an empty hdu and prepare output*/
        long totpix = naxes[0] * naxes[1] * naxes[2] * naxes[3] * naxes[4] * naxes[5] * naxes[6] * naxes[7] * naxes[8];
        if (hdutype != IMAGE_HDU ){
            outputDesc[iHdu].clear();
            outputDesc[iHdu]="not an image HDU";
            continue;
        }
        if(naxis == 0 || totpix == 0){
            outputDesc[iHdu].clear();
            outputDesc[iHdu]="empty HDU";
            continue;
        }
        outputDesc[iHdu].clear();
        outputDesc[iHdu]="image HDU";
        
        /*check if region is outside image limits*/
        if(fpixel[0]<1 || fpixel[0]>naxes[0] || lpixel[0]<1 || lpixel[0]>naxes[0] || fpixel[1]<1 || fpixel[1]>naxes[1] || lpixel[1]<1 || lpixel[1]>naxes[1]){
            cerr << red << "\nError: the region is outside image limits!\n\n" << normal;
            continue;
        }
        
                
        /*read region in HDU*/
        fits_read_subset(infptr, TDOUBLE, fpixel, lpixel, inc, &nulval, array, &anynul, &status);
        vector<double> vArray(nPix);
        for (unsigned int iPix=0;iPix<nPix;iPix++) vArray[iPix]=array[iPix];
        
        if(kMode=="Mean"){
            /*compute mean*/
            double mean;
            computeMean(vArray,mean);
            output[iHdu][0] = mean;
        }
        
        if(kMode=="Median"){
            /*compute median*/
            double median;
            computeMedian(vArray,median);
            output[iHdu][0] = median;
        }
        if(kMode=="Stdev"){
            /*compute std*/
            double stdev;
            computeStd(vArray,stdev);
            output[iHdu][0] = stdev;
        }
        if(kMode=="Mad"){
            /*compute mad*/
            double mad;
            computeMad(vArray,mad);
            output[iHdu][0] = mad;
        }
        if(kMode=="All"){
            /*compute mean*/
            double mean;
            computeMean(vArray,mean);
            output[iHdu][0] = mean;
            /*compute median*/
            double median;
            computeMedian(vArray,median);
            output[iHdu][1] = median;
            /*compute std*/
            double stdev;
            computeStd(vArray,stdev);
            output[iHdu][2] = stdev;
            /*compute mad*/
            double mad;
            computeMad(vArray,mad);
            output[iHdu][3] = mad;
        }        
    }
    
    /* Close the output file */
    fits_close_file(infptr, &status);
    delete array;
    return status;
}


int processCommandLineArgs(const int argc, char *argv[], string &inFile, vector<int> &singleHdu, long *fpixel, long *lpixel, string &kMode, unsigned int &nOutVars, bool &verbosity){

    if(argc == 1) return 1;
    
    /*default option*/
    kMode = "All";
    nOutVars = 4;
    
    bool KModeFlag = false;
    int opt=0;
    while ( (opt = getopt(argc, argv, "mnopas:qQhH?")) != -1) {
        switch (opt) {
                
            case 's':
                
                singleHdu.push_back(atoi(optarg));
                break;
            case 'm':
                if(!KModeFlag){
                    KModeFlag = true;
                    kMode = "Mean";
                    nOutVars = 1;
                } else{
                    cerr << red << "\nError, only one statistic option or none!\n\n" << normal;
                    return 1;
                }
                break;
            case 'n':
                if(!KModeFlag){
                    KModeFlag = true;
                    kMode = "Median";
                    nOutVars = 1;
                }else{
                    cerr << red << "\nError, only one statistic option or none!\n\n" << normal;
                    return 1;
                }
                break;
            case 'o':
                if(!KModeFlag){
                    KModeFlag = true;
                    kMode = "Stdev";
                    nOutVars = 1;
                }else{
                    cerr << red << "\nError, only one statistic option or none!\n\n" << normal;
                    return 1;
                }
                break;
            case 'p':
                if(!KModeFlag){
                    KModeFlag = true;
                    kMode = "Mad";
                    nOutVars = 1;
                } else{
                    cerr << red << "\nError, only one statistic option or none!\n\n" << normal;
                    return 1;
                }
                break;
            case 'a':
                if(!KModeFlag){
                    KModeFlag = true;
                    kMode = "All";
                    nOutVars = 4;
                } else{
                    cerr << red << "\nError, only one statistic option or none!\n\n" << normal;
                    return 1;
                }
                break;
            case 'Q':
            case 'q':
                verbosity = 0;
                break;
            case 'h':
            case 'H':
            default: /* '?' */
                return 1;
        }
    }
    
    if (!(argc-optind==2)){
        cerr << red << "Error: no input file(s) provided!\n\n" << normal;
        return 1;
    }
    
    /*load input file name*/
    inFile=argv[optind];
    
    /*load region limits to process*/
    string sRegCoor = argv[optind +1];
    replace(sRegCoor.begin(), sRegCoor.end(), '[', ' ');
	replace(sRegCoor.begin(), sRegCoor.end(), ':', ' ');
	replace(sRegCoor.begin(), sRegCoor.end(), ',', ' ');
	replace(sRegCoor.begin(), sRegCoor.end(), ']', ' ');
    istringstream regCoor(sRegCoor.c_str());
    long xMin, xMax, yMin, yMax;
    regCoor >> xMin;
    if (regCoor.fail()) {
        cerr << red << "Bad region limits 1!\n\n" << normal;
        return 1;
    }
    regCoor >> xMax;
    if (regCoor.fail()) {
        cerr << red << "Bad region limits 2!\n\n" << normal;
        return 1;
    }
    regCoor >> yMin;
    if (regCoor.fail()) {
        cerr << red << "Bad region limits 3!\n\n" << normal;
        return 1;
    }
    regCoor >> yMax;
    if (regCoor.fail()) {
        cerr << red << "Bad region limits 4!\n\n" << normal;
        return 1;
    }
    fpixel[0]=xMin;
    fpixel[1]=yMin;
    lpixel[0]=xMax;
    lpixel[1]=yMax;
    
    
    return 0;
}

int main(int argc, char *argv[])
{
    
    string inFile;
    
    /*input options*/
    vector<int> singleHdu;
    long fpixel[2], lpixel[2];
    unsigned int nOutVars;
    string kMode;
    int returnCode = processCommandLineArgs( argc, argv,inFile, singleHdu, fpixel, lpixel, kMode, nOutVars,verbosity);
    if(returnCode!=0){
        if(returnCode == 1) printCopyHelp(argv[0],true);
        return returnCode;
    }
    
    /* Overwrite the output file if it already exist */
    if(!fileExist(inFile.c_str())){
        cerr << yellow << "\nThe input file doesn't exist. " << normal;
    }
        
    /* Do the actual processing */
    vector< vector <double> > output;
    vector<string> outputDesc;
    int status = noiseSample(inFile.c_str(),singleHdu,fpixel,lpixel,kMode, output, outputDesc, nOutVars);
    if (status != 0){
        fits_report_error(stderr, status);
        return status;
    }
    
    
    //print output
    int columnWidth = 13;
    if (verbosity) {
        cout << endl;
        cout << "=================================================================" << endl;
        cout << "Input file: " << inFile.c_str()<<endl;
        cout << "Region = " << "[" << fpixel[0] << ":" << lpixel[0] << "," << fpixel[1] << "," << lpixel[1] << "]" << endl;
        cout << "=================================================================" << endl;
        if (kMode=="All"){
            cout <<setw(3) << "HDU" << setw(columnWidth) << "Mean" << setw(columnWidth) << "Median" << setw(columnWidth) << "Std. Dev." << setw(columnWidth) << "Mad" << setw (18) << "HDU description"<< endl;
        } else{
            cout << setw(3) << "HDU" << setw (columnWidth) << kMode << setw (18) << "HDU description"<< endl;
        }
        for(unsigned int iHdu=0;iHdu<singleHdu.size();iHdu++) {
            cout << setw(3) << singleHdu[iHdu];
            for (unsigned int iOutVar = 0;iOutVar<output[iHdu].size();iOutVar++){
                cout << setw (columnWidth) << setprecision(2) << fixed << output[iHdu][iOutVar];
            }
            cout << setw (18) << outputDesc[iHdu] << endl;
        }
        cout << endl;
    } else {
        for(unsigned int iHdu=0;iHdu<singleHdu.size();iHdu++) {
            cout << singleHdu[iHdu] << " ";
            for (unsigned int iOutVar = 0;iOutVar<output[iHdu].size();iOutVar++){
                cout << output[iHdu][iOutVar] << " ";
            }
            cout << endl;
        }
    }
    
    
    return status;
}
