#include <string.h>
#include <stdio.h>
#include "fitsio.h"

#include <iostream>
#include <sstream>

#include <inttypes.h>
#include <fstream>
#include <unistd.h>
#include <getopt.h>    /* for getopt_long; standard getopt is in unistd.h */
#include <vector>
#include <algorithm>
#include <cmath>
#include <iomanip>


int gVerbosity = true;
//colors
const char cyan[] = { 0x1b, '[', '1', ';', '3', '6', 'm', 0 };
const char magenta[] = { 0x1b, '[', '1', ';', '3', '5', 'm', 0 };
const char red[] = { 0x1b, '[', '1', ';', '3', '1', 'm', 0 };
const char green[] = { 0x1b, '[', '1', ';', '3', '2', 'm', 0 };
const char yellow[] = { 0x1b, '[', '1', ';', '3', '3', 'm', 0 };
const char blue[] = "\x1b[1;34m";

const char bold[] = "\x1b[1;39m";

const char whiteOnRed[]    = "\x1b[1;41m";
const char whiteOnGreen[]  = "\x1b[1;42m";
const char whiteOnPurple[] = "\x1b[1;45m";
const char whiteOnViolet[] = "\x1b[1;44m";
const char whiteOnBrown[]  = "\x1b[1;43m";
const char whiteOnGray[]   = "\x1b[1;47m";

const char normal[] = { 0x1b, '[', '0', ';', '3', '9', 'm', 0 };


using namespace std;

int deleteFile(const char *fileName){
    cout << yellow;
    cout << "Will overwrite: " << fileName << endl << endl;
    cout << normal;
    return unlink(fileName);
}

bool fileExist(const char *fileName){
    ifstream in(fileName,ios::in);
    
    if(in.fail()){
        //cout <<"\nError reading file: " << fileName <<"\nThe file doesn't exist!\n\n";
        in.close();
        return false;
    }
    
    in.close();
    return true;
}

void printCopyHelp(const char *exeName, bool printFullHelp=false){
    
    if(printFullHelp){
        cout << bold;
        cout << endl;
        cout << "This program reads a keyword from the file header";
        cout << normal;
    }
    cout << "==========================================================================\n";
    cout << yellow;
    cout << "\nUsage:\n";
    cout << "  "   << exeName << "-i <input file> -s <HDU number> -k <KEYWORD> \n";
    cout << "\nOptions:\n";
    cout << "  -q for quiet (no screen output)\n";
    cout << "  -s <HDU number>\n";
    cout << "  -k <KEYWORD> key word name\n";
    cout << "  -v print value\n";
    cout << "  -r print entire record\n";
    cout << "  -c print comment\n";
    cout << normal;
    cout << "==========================================================================\n\n";
}

void computeMean(double *array,double &mean){
    double sum=0;
    const long N = sizeof(array)/sizeof(array[0]);
    for(unsigned int i=0;i<N;i++){
        sum=+array[i];
    }
    mean = sum/N;
}
void computeStd(double *array,double &stdev){
    double mean;
    computeMean(array,mean);
    
    const long N = sizeof(array)/sizeof(array[0]);
    double quadSum=0;
    for(unsigned int i=0;i<N;i++){
        quadSum =+ pow(array[i]-mean,2.0);
    }
    
    stdev = sqrt(quadSum/N);
}

void computeMedian(vector<double>vArray,double &median){
    
    const long N = vArray.size();
    bool NOdd = !!(N & 1);
    const long m = N/2;
    if (NOdd){
        std::nth_element(vArray.begin(), vArray.begin()+vArray.size()/2, vArray.end());
        median = vArray[m];
    }else {
        std::nth_element(vArray.begin(), vArray.begin()+vArray.size()/2, vArray.end());
        const double highMedianVal = vArray[m];
        std::nth_element(vArray.begin(), vArray.begin()+vArray.size()/2-1, vArray.end());
        const double lowMedianVal  = vArray[m-1];
        median = (highMedianVal+lowMedianVal)/2.;
        
    }
}

void computeMad(vector<double> &vArray,double &mad){
    double median;
    computeMedian(vArray, median);
    
    const long N = vArray.size();
    for(unsigned int i=0;i<N;i++){
        //ver si fabs() es para doubles
        vArray[i] = fabs(vArray[i]-median);
    }
    
    bool NOdd = !!(N & 1);
    const long m = N/2;
    if (NOdd){
        std::nth_element(vArray.begin(), vArray.begin()+vArray.size()/2, vArray.end());
        mad = vArray[m];
    }else {
        std::nth_element(vArray.begin(), vArray.begin()+vArray.size()/2, vArray.end());
        const double highMadVal = vArray[m];
        std::nth_element(vArray.begin(), vArray.begin()+vArray.size()/2-1, vArray.end());
        const double lowMadVal  = vArray[m-1];
        mad = (highMadVal+lowMadVal)/2.;
        
    }
    
    
}

int noiseSample(const char *inF, vector<int> &singleHdu, long *fpixel, long *lpixel, const string kMode, vector< vector <double> > &output, vector<string> &outputDesc, const unsigned int nOutVars){
  
    fitsfile *infptr;   /* FITS file pointers defined in fitsio.h */
    
    int status = 0;
    int nhdu = 0;
    
    /*open fits file*/
    fits_open_file(&infptr, inF, READONLY, &status); /* Open the input file */
    if (status != 0) return(status);
    
    /* check extensions to process*/
    fits_get_num_hdus(infptr, &nhdu, &status);
    
    if(singleHdu.size() == 0){
        for(int i=0;i<nhdu;++i){
            singleHdu.push_back(i+1);
        }
    }
    
    for(unsigned int i=0;i<singleHdu.size();++i){
        if(singleHdu[i] > nhdu){
            fits_close_file(infptr,  &status);
            cerr << red << "\nError: the file does not have the required HDU!\n\n" << normal;
            return -1000;
        }
    }
    
    const unsigned nUseHdu = singleHdu.size();
    
    /* check region to process*/
    int hdutype, bitpix, naxis = 0;
    long naxes[9] = {1, 1, 1, 1, 1, 1, 1, 1, 1};
    for (unsigned int i=0;i<nUseHdu;i++){
        
        fits_movabs_hdu(infptr, i, &hdutype, &status);
        for (int j = 0; j < 9; ++j) naxes[j] = 1;
        fits_get_img_param(infptr, 9, &bitpix, &naxis, naxes, &status);
        if(fpixel[0]<1 || fpixel[0]>naxes[0] || lpixel[0]<1 || lpixel[0]>naxes[0] || fpixel[1]<1 || fpixel[1]>naxes[1] || lpixel[1]<1 || lpixel[1]>naxes[1]){
            fits_close_file(infptr,  &status);
            cerr << red << "\nError: the region is outside image limits!\n\n" << normal;
            return -1000;
        }
    }
    
    /*prepare output*/
    vector<double> temp;
    temp.assign(nOutVars,NAN);
    output.assign(nUseHdu,temp);
    outputDesc.assign(nUseHdu,"empty string");
    
    /* process region of interest*/
    long nPix = (lpixel[0]-fpixel[0])*(lpixel[1]-fpixel[1]);
    long inc[2] = {1,1};
    double nulval = 0.;
    int anynul;
    double *array = new double[nPix];
    for (unsigned int iHdu=0; iHdu<nUseHdu;iHdu++){ /*main loop through each extension*/
        
        /*moves to HDU*/
        const unsigned int n = singleHdu[iHdu];
        fits_movabs_hdu(infptr, n, &hdutype, &status);
        for (int j = 0; j < 9; ++j) naxes[j] = 1;
        fits_get_img_param(infptr, 9, &bitpix, &naxis, naxes, &status);
        
        /*check if it is an empty hdu and prepare output*/
        long totpix = naxes[0] * naxes[1] * naxes[2] * naxes[3] * naxes[4] * naxes[5] * naxes[6] * naxes[7] * naxes[8];
        if (hdutype != IMAGE_HDU ){
            outputDesc[iHdu]="not an image HDU";
            continue;
        }
        if(naxis == 0 || totpix == 0){
            outputDesc[iHdu]="empty HDU";
            continue;
        }
        outputDesc[iHdu]="image HDU";
                
        /*read region in HDU*/
        fits_read_subset(infptr, TDOUBLE, fpixel, lpixel, inc, &nulval, array, &anynul, &status);
        
        if(kMode=="Mean"){
            /*compute mean*/
            double mean;
            computeMean(array,mean);
            output[iHdu].push_back(mean);
        }
        
        if(kMode=="Median"){
            /*compute median*/
            vector<double> vArray(nPix);
            for (unsigned int iPix=0;iPix<nPix;iPix++) vArray[iPix]=array[iPix];
            double median;
            computeMedian(vArray,median);
            output[iHdu].push_back(median);
        }
        if(kMode=="Sdted"){
            /*compute std*/
            double stdev;
            computeStd(array,stdev);
            output[iHdu].push_back(stdev);
        }
        if(kMode=="Mad"){
            /*compute mad*/
            vector<double> vArray(nPix);
            for(unsigned int iPix=0;iPix<nPix;iPix++) vArray[iPix]=array[iPix];
            double mad;
            computeMad(vArray,mad);
            output[iHdu].push_back(mad);
        }
        if(kMode=="All"){
            /*compute mean*/
            double mean;
            computeMean(array,mean);
            output[iHdu].push_back(mean);
            /*compute median*/
            vector<double> vArray(nPix);
            for(unsigned int iPix=0;iPix<nPix;iPix++) vArray[iPix]=array[iPix];
            double median;
            computeMedian(vArray,median);
            output[iHdu].push_back(median);
            /*compute std*/
            double stdev;
            computeStd(array,stdev);
            output[iHdu].push_back(stdev);
            /*compute mad*/
            double mad;
            computeMad(vArray,mad);
            output[iHdu].push_back(mad);
        }
//escribir las salidas en el vector de doubles
        
    }
    /* Close the output file */
    fits_close_file(infptr, &status);
    delete array;
    //fijarse si hay que borrar los vectores
    return status;
}


int processCommandLineArgs(const int argc, char *argv[], string inFile, vector<int> singleHdu, long *fpixel, long *lpixel, string &kMode, unsigned int &nOutVars, bool &verbosity){

    if(argc == 1) return 1;
    
    kMode = "All";
    bool KModeFlag = false;
    int opt=0;
    while ( (opt = getopt(argc, argv, "mnopas:qQhH?")) != -1) {
        switch (opt) {
                
            case 's':
                
                singleHdu.push_back(atoi(optarg));
                break;
            case 'm':
                if(!KModeFlag){
                    KModeFlag = true;
                    kMode = "Mean";
                    nOutVars = 1;
                } else{
                    cerr << red << "\nError, only one statistic option or none!\n\n" << normal;
                    return 1;
                }
                break;
            case 'n':
                if(!KModeFlag){
                    KModeFlag = true;
                    kMode = "Median";
                    nOutVars = 1;
                }else{
                    cerr << red << "\nError, only one statistic option or none!\n\n" << normal;
                    return 1;
                }
                break;
            case 'o':
                if(!KModeFlag){
                    KModeFlag = true;
                    kMode = "Stdev";
                    nOutVars = 1;
                }else{
                    cerr << red << "\nError, only one statistic option or none!\n\n" << normal;
                    return 1;
                }
                break;
            case 'p':
                if(!KModeFlag){
                    KModeFlag = true;
                    kMode = "Mad";
                    nOutVars = 1;
                } else{
                    cerr << red << "\nError, only one statistic option or none!\n\n" << normal;
                    return 1;
                }
                break;
            case 'a':
                if(!KModeFlag){
                    KModeFlag = true;
                    kMode = "All";
                    nOutVars = 4;
                } else{
                    cerr << red << "\nError, only one statistic option or none!\n\n" << normal;
                    return 1;
                }
                break;
            case 'Q':
            case 'q':
                verbosity = 0;
                break;
            case 'h':
            case 'H':
            default: /* '?' */
                return 1;
        }
    }
    
    if (!(argc-optind==2)){
        cerr << red << "Error: no input file(s) provided!\n\n" << normal;
        return 1;
    }
    
    /*load input file name*/
    inFile=argv[optind];
    
    /*load region limits to process*/
    string sRegCoor = argv[optind +1];
    replace(sRegCoor.begin(), sRegCoor.end(), '[', ' ');
	replace(sRegCoor.begin(), sRegCoor.end(), ':', ' ');
	replace(sRegCoor.begin(), sRegCoor.end(), ',', ' ');
	replace(sRegCoor.begin(), sRegCoor.end(), ']', ' ');
    istringstream regCoor(sRegCoor.c_str());
    long xMin, xMax, yMin, yMax;
    regCoor >> xMin;
    if (std::istringstream::failbit) {
        cerr << red << "Bad region limits!\n\n" << normal;
        return 1;
    }
    regCoor >> xMax;
    if (std::istringstream::failbit) {
        cerr << red << "Bad region limits!\n\n" << normal;
        return 1;
    }
    regCoor >> yMin;
    if (std::istringstream::failbit) {
        cerr << red << "Bad region limits!\n\n" << normal;
        return 1;
    }
    regCoor >> yMax;
    if (std::istringstream::failbit) {
        cerr << red << "Bad region limits!\n\n" << normal;
        return 1;
    }
    fpixel[0]=xMin;
    fpixel[1]=yMin;
    lpixel[0]=xMax;
    lpixel[1]=yMax;
    
    
    return 0;
}

int main(int argc, char *argv[])
{
    
    string inFile;
    
    /*input options*/
    vector<int> singleHdu;
    long fpixel[2], lpixel[2];
    unsigned int nOutVars;
    string kMode;
    bool verbosity;
    int returnCode = processCommandLineArgs( argc, argv,inFile, singleHdu, fpixel, lpixel, kMode, nOutVars,verbosity);
    if(returnCode!=0){
        if(returnCode == 1) printCopyHelp(argv[0],true);
        return returnCode;
    }
    
    /* Overwrite the output file if it already exist */
    if(!fileExist(inFile.c_str())){
        cerr << yellow << "\nThe input file doesn't exist. " << normal;
    }
        
    /* Do the actual processing */
    vector< vector <double> > output;
    vector<string> outputDesc;
    int status = noiseSample(inFile.c_str(),singleHdu,fpixel,lpixel,kMode, output, outputDesc, nOutVars);
    if (status != 0){
        fits_report_error(stderr, status);
        return status;
    }
    
    //print output
    if (verbosity) {
        cout << "=================================================" << endl;
        cout << "Input file: " << inFile.c_str()<<endl;
        cout << "Region = " << "[" << fpixel[0] << ":" << lpixel[0] << "," << fpixel[1] << "," << lpixel[1] << "]" << endl;
        cout << "=================================================" << endl;
        if (kMode=="All"){
            cout <<setw(3) << "HDU" << setw(12) << "Mean" << setw(12) << "Median" << setw(12) << "Std. Dev." << setw(12) << "Mad" << endl;
        } else{
            cout << setw(3) << "HDU" << setw (12) << kMode << setw (18) << "HDU description"<< endl;
        }
        for(unsigned int iHdu=0;iHdu<singleHdu.size();iHdu++) {
            cout << singleHdu[iHdu] << "  ";
            for (unsigned int iOutVar = 0;iOutVar<output[iHdu].size();iOutVar++){
                cout << output[iHdu][iOutVar] << "  ";
            }
            cout << outputDesc[iHdu] << endl;
        }
    } else {
        for(unsigned int iHdu=0;iHdu<singleHdu.size();iHdu++) {
            cout << singleHdu[iHdu] << " ";
            for (unsigned int iOutVar = 0;iOutVar<output[iHdu].size();iOutVar++){
                cout << output[iHdu][iOutVar] << " ";
            }
            cout << endl;
        }
    }
    
    
    
    return status;
}
