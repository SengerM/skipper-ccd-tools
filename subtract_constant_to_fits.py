DEFAULT_RESULT_FILE_NAME = 'result.fits'

########################################################################

from astropy.io import fits
import numpy as np
import argparse

parser = argparse.ArgumentParser(description = 'Subtract a constant value to a FITS file.')
parser.add_argument('--fits',
	metavar = 'file', 
	help = 'Input FITS image for the operation.',
	required = True,
	dest = 'fits'
)
parser.add_argument('--ofile',
	metavar = 'file name', 
	help = 'Name of the resulting image file. Default is "' + DEFAULT_RESULT_FILE_NAME + '".',
	dest = 'ofile'
)
parser.add_argument('--value',
	metavar = 'value', 
	help = 'Value to subtract.',
	dest = 'val',
	type = float,
	required = True,
)

args = parser.parse_args()

with fits.open(args.fits) as hdul:
	for k in range(len(hdul)):
		hdul[k].data = hdul[k].data - args.val
	ofilename = DEFAULT_RESULT_FILE_NAME if args.ofile == None else args.ofile
	ofilename += '.fits' if ofilename[-5:] != '.fits' else ''
	hdul.writeto(args.fits[:args.fits.rfind('/')+1] + ofilename)
