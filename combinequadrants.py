import os
import sys

INKSCAPE_TOOLS_PATH = '/home/darmat/soft/skipper-ccd-tools/inkscape-tools/' # This path must end with a '/' !!!!!!!!
INKSCAPE_TEMPLATE = 'template-4-amps-without-overscan.svg' #'template-two-amps-ccd-without-overscan.svg'

directory_or_file = sys.argv[1]

def process_file(filepath):
	for k in range(4):
		if filepath[filepath.rfind('/')+1:-5] + '_' + str(k) + '.jpeg' not in os.listdir(filepath[:filepath.rfind('/')]):
			raise ValueError('You first have to run "fits2jpeg"')

	for k in range(4):
		os.system('cp ' + filepath[:-5] + '_' + str(k) + '.jpeg ' + INKSCAPE_TOOLS_PATH + 'cuadrant_' + str(k) + '.jpeg')

	os.system('inkscape ' + INKSCAPE_TOOLS_PATH + INKSCAPE_TEMPLATE + ' --export-png=' + INKSCAPE_TOOLS_PATH + 'template.png --export-area-page')
	os.system('convert ' + INKSCAPE_TOOLS_PATH + 'template.png -quality 20 ' + INKSCAPE_TOOLS_PATH + 'template.jpg')
	os.system('mv ' + INKSCAPE_TOOLS_PATH + 'template.jpg ' + filepath[:filepath.rfind('/')+1] + filepath[filepath.rfind('/')+1:-5] + '_combined.jpg')
	os.system('rm ' + INKSCAPE_TOOLS_PATH + 'template.png')


if '.fits' in directory_or_file:
	process_file(directory_or_file)
else:
	if directory_or_file[-1] != '/':
		directory_or_file += '/'
	for file in os.listdir(directory_or_file):
		if '.fits' in file:
			process_file(directory_or_file + file)
